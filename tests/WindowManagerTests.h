#pragma once

#include <QtCore/QObject>
#include <QtTest/QtTest>
#include <QtWidgets/QApplication>
#include <QMessageBox>
#include <QPushButton>


#include "WindowManager.h"

class WindowManagerTests : public QObject
{
    Q_OBJECT
   private:
    int argc = 0;
    QApplication* app;

   private slots:
    void initTestCase();
    void shouldOpenRenderingWindow();
    void shouldOpenRenderingInputWidget();
    void shouldOpenRenderingWidgetAndDisplayImage();

   public:
    ~WindowManagerTests();
};

