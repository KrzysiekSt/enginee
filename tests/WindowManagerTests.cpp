#include "WindowManagerTests.h"

void WindowManagerTests::initTestCase()
{
    char *argv[] = {nullptr};
    app = new QApplication(argc, argv);
}

void WindowManagerTests::shouldOpenRenderingWindow()
{
    // given

    // when
    WindowManager::createRenderingWindow(std::make_shared<ImageProcessorData>());
    // then
    QEventLoop loop;
    QTimer::singleShot(500, &loop, &QEventLoop::quit);
    loop.exec();
    QVERIFY2(WindowManager::rendering_window->isVisible(), "should create rendering window");
    WindowManager::rendering_window->close();
    WindowManager::clear();
}

void WindowManagerTests::shouldOpenRenderingInputWidget()
{
    // given

    // when
    WindowManager::createInputWidget(); // It's manual test (click show model)

    // then
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(nullptr, "Test", "Did input widget appear as expected?", QMessageBox::Yes|QMessageBox::No);
    QVERIFY2(reply == QMessageBox::Yes, "User confirmed that test passed");
    WindowManager::clear();
}


void WindowManagerTests::shouldOpenRenderingWidgetAndDisplayImage()
{
    // given
    auto rendering_data = WindowManager::createInputWidget();
    WindowManager::createRenderingWindow(rendering_data);

    // when
    WindowManager::startWorkerLoop(); // It's manual test (add cube and show model)

    // then
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(nullptr, "Test", "Did rendering widget appear as expected and display image?", QMessageBox::Yes|QMessageBox::No);
    QVERIFY2(reply == QMessageBox::Yes, "User confirmed that test passed");
    WindowManager::clear();
}

WindowManagerTests::~WindowManagerTests()
{
    delete app;
}

QTEST_APPLESS_MAIN(WindowManagerTests)