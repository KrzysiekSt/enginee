#pragma once

#include <QtCore/QObject>
#include <QtTest/QtTest>

#include "image_creating/ImageProcessor.h"
#include "image_creating/Image_processor_stages/rendering/OpenGLRenderingStage.h"

class ImageProcessorTests : public QObject
{
    Q_OBJECT

   private slots:
    void shouldFillRenderingData();
    void shouldRenderImage();
};
