#include "ImageProcessorTests.h"

void ImageProcessorTests::shouldFillRenderingData()
{
    // given
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    ImageProcessor renderer(std::move(stages));
    std::shared_ptr<ImageProcessorData> input_data = std::make_shared<ImageProcessorData>();
    // when
    renderer.assignData(input_data);
    // then
    std::weak_ptr<ImageProcessorData> output_data = renderer.getCurrentRenderingData();
    QVERIFY2(output_data.lock() == input_data, "should fill render output_data");
}

void ImageProcessorTests::shouldRenderImage()
{
    // given
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    stages.emplace_back(std::make_unique<OpenGLRenderingStage>());
    ImageProcessor renderer(std::move(stages));
    renderer.assignData(std::make_shared<ImageProcessorData>());

    // when
    QImage render_target = renderer.renderFrame();

    // then
    QVERIFY2(!render_target.isNull(), "should generate image");
}

QTEST_MAIN(ImageProcessorTests)