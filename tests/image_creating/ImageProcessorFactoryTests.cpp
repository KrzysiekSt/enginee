#include "ImageProcessorFactoryTests.h"

void ImageProcessorFactoryTests::shouldReturnRenderingPipeline()
{
    //given

    //when
    std::shared_ptr<ImageProcessor> rendering_pipeline = ImageProcessorFactory::createOpenGLImageProcessor();

    //then
    QVERIFY2(rendering_pipeline, "in not nullptr");
}
void ImageProcessorFactoryTests::shouldReturnWorkingRenderingPipeline()
{
    //given
    std::shared_ptr<ImageProcessor> rendering_pipeline = ImageProcessorFactory::createOpenGLImageProcessor();
    rendering_pipeline->assignData(std::make_shared<ImageProcessorData>());

    //when
    QImage frame = rendering_pipeline->renderFrame();

    //then
    QVERIFY2(!frame.isNull(), "is not null");
}

QTEST_MAIN(ImageProcessorFactoryTests)
