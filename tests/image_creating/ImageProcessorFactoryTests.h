#pragma once

#include <QtCore/QObject>
#include <QtTest/QtTest>

#include "image_creating/ImageProcessorFactory.h"
#include "image_creating/ImageProcessor.h"

class ImageProcessorFactoryTests : public QObject
{
    Q_OBJECT

   private slots:
    void shouldReturnRenderingPipeline();
    void shouldReturnWorkingRenderingPipeline();


};

