
cd ./third_party/Qt_source
sudo dnf install libxkbcommon-devel libxkbcommon libxkbcommon-x11-devel libxkbfile-devel xkbcomp xkbcomp-devel xkbset xcb-util-renderutil libxcb xcb-util-xrm-devel xcb-util-wm-devel xcb-util-renderutil-devel xcb-util-keysyms-devel libxcb libxcb-devel xcb-util xcb-util-devel libX11-devel libXrender-devel libxkbcommon-devel libxkbcommon-x11-devel libXi-devel libdrm-devel libXcursor-devel libXcomposite-devel xcb-util-image-devel xcb-util-cursor-devel libxcb-devel libX11-xcb fontconfig-devel fontconfig
sudo dnf install glslang
sudo dnf install mesa-vulkan-drivers
./configure -opensource -confirm-license -static -static-runtime -debug -platform linux-g++-64 -feature-freetype -fontconfig -feature-wayland-server -nomake examples -prefix ../Qt-6.5.3
cmake --build . --parallel
cmake --install .
git clean -dfx
git restore .
git submodule foreach --recursive git clean -dfx
git submodule foreach --recursive git restore .