SETX VULKAN_SDK "C:\VulkanSDK\1.3.268.0"
cd ./third_party/Qt_source
CALL "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64
SET _ROOT=%CD%
SET PATH=%_ROOT%;%PATH%
SET _ROOT=
CALL .\configure.bat -opensource -confirm-license -static -static-runtime -debug -platform win32-msvc -nomake examples -skip qtquicktimeline,qtmultimedia,qtspeech,qtquick3d,qtgraphs,qtquick3dphysics -prefix ../Qt-6.5.3
cmake --build . --parallel
cmake --install .
git clean -dfx
git restore .
git submodule foreach --recursive git clean -dfx
git submodule foreach --recursive git restore .
cd ../../