#include "WindowManager.h"

std::shared_ptr<ImageProcessorData> WindowManager::createInputWidget()
{
    input_widget = std::make_unique<InputWidget>();
    return input_widget->getInput();
}

void WindowManager::startWorkerLoop()
{
    rendering_window->run();
}

void WindowManager::createOpenGlRenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data)
{
    const QString RENDERING_WIDGET_TITLE = "OpenGL rendering widget: %1";

    rendering_window = std::make_unique<TestWindow>(rendering_data, rasterization_factory.createOpenGLImageProcessor());
    rendering_window->setTitle(RENDERING_WIDGET_TITLE.arg(1));
    rendering_window->show();
}

void WindowManager::createVulkanRenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data)
{
    const QString RENDERING_WIDGET_TITLE = "Vulkan rendering widget: %1";

    rendering_window = std::make_unique<TestWindow>(rendering_data, rt_factory.createVulkanImageProcessor());
    rendering_window->setTitle(RENDERING_WIDGET_TITLE.arg(1));
    rendering_window->show();
}

void WindowManager::createCPURenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data)
{
    const QString RENDERING_WIDGET_TITLE = "CPU rendering widget: %1";

    rendering_window = std::make_unique<TestWindow>(rendering_data, rt_factory.createCPUImageProcessor());
    rendering_window->setTitle(RENDERING_WIDGET_TITLE.arg(1));
    rendering_window->show();
}
