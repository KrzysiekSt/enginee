#include <QApplication>
#include <QDebug>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWindow>

constexpr static QSize WIDGET_SIZE = {1280, 720};

class TestWindow : public QOpenGLWindow, protected QOpenGLFunctions
{
    QOpenGLShaderProgram shaders;

    std::array<float, 6> positions = {-0.5, -0.5, 0.5, 0.5, -0.5, 0.5};
    unsigned int         buffer;
    const int            vertex_attribute_index    = 0;
    const int            length_of_position_vector = 2;
    const int            size_of_vertex_data       = length_of_position_vector * sizeof(float);
    const int            offset_to_attribute       = 0;

    QOpenGLBuffer vertex_buffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);

    void initializeGL() override
    {
        initializeOpenGLFunctions();

        vertex_buffer.create();
        vertex_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
        vertex_buffer.bind();
        vertex_buffer.allocate(positions.data(), positions.size() * sizeof(float));

        QSurfaceFormat format;
        format.setMajorVersion(4);
        format.setMinorVersion(6);
        format.setProfile(QSurfaceFormat::CompatibilityProfile);

        setFormat(format);
        create();

        if(!shaders.addShaderFromSourceCode(QOpenGLShader::Vertex, R"(
            #version 330 core

            layout(location = 0) in vec4 position;

            void main()
            {
                gl_Position = position;
            }
        )"))
        {
            qDebug() << "Vertex shader errors:\n" << shaders.log();
        }

        if(!shaders.addShaderFromSourceCode(QOpenGLShader::Fragment, R"(
        #version 330 core

        layout(location = 0) out vec4 color;

        void main()
        {
            color = vec4(1.0, 0.0, 0.0, 1.0);
        }

    )"))
        {
            qDebug() << "Fragment shader errors:\n" << shaders.log();
        }

        if(!shaders.link())
        {
            qDebug() << "Shader linker errors:\n" << shaders.log();
            shaders.removeAllShaders();
        }
        shaders.enableAttributeArray(vertex_attribute_index);
        shaders.setAttributeBuffer(
            vertex_attribute_index,
            GL_FLOAT,
            offset_to_attribute,
            length_of_position_vector,
            size_of_vertex_data
        );
        shaders.bind();
    }

    void paintGL() override
    {
        makeCurrent();
        // set the background color = clear color
        glClearColor(0.1f, 0.1f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

  public:
    ~TestWindow()
    {
        makeCurrent();
        vertex_buffer.destroy();
        shaders.release();
        shaders.removeAllShaders();
    }
};

int main(int argc, char* argv[])
{
    QApplication test_app(argc, argv);

    TestWindow new_rendering_window;
    new_rendering_window.resize(WIDGET_SIZE);
    new_rendering_window.setTitle("Test window");
    new_rendering_window.show();

    return test_app.exec();
}