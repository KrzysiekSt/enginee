#include <QApplication>
#include <QDebug>
#include <QOffscreenSurface>
#include <QOpenGLBuffer>
#include <QOpenGLContext>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QRasterWindow>

int main(int argc, char* argv[])
{
    QApplication test_app(argc, argv);

    QOpenGLContext context;
    context.create();

    QOffscreenSurface surface;
    surface.create();

    context.makeCurrent(&surface);
    QOpenGLShaderProgram shaders;

    std::array<float, 6> positions = {-0.5, -0.5, 0.5, 0.5, -0.5, 0.5};
    unsigned int         buffer;
    const int            vertex_attribute_index    = 0;
    const int            length_of_position_vector = 2;
    const int            size_of_vertex_data       = length_of_position_vector * sizeof(float);
    const int            offset_to_attribute       = 0;

    QOpenGLBuffer vertex_buffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    vertex_buffer.create();
    vertex_buffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
    vertex_buffer.bind();
    vertex_buffer.allocate(positions.data(), positions.size() * sizeof(float));

    if(!shaders.addShaderFromSourceCode(QOpenGLShader::Vertex, R"(
            #version 330 core

            layout(location = 0) in vec4 position;

            void main()
            {
                gl_Position = position;
            }
        )"))
    {
        qDebug() << "Vertex shader errors:\n" << shaders.log();
    }

    if(!shaders.addShaderFromSourceCode(QOpenGLShader::Fragment, R"(
        #version 330 core

        layout(location = 0) out vec4 color;

        void main()
        {
            color = vec4(1.0, 0.0, 0.0, 1.0);
        }

    )"))
    {
        qDebug() << "Fragment shader errors:\n" << shaders.log();
    }

    if(!shaders.link())
    {
        qDebug() << "Shader linker errors:\n" << shaders.log();
        shaders.removeAllShaders();
    }
    shaders.enableAttributeArray(vertex_attribute_index);
    shaders.setAttributeBuffer(
        vertex_attribute_index,
        GL_FLOAT,
        offset_to_attribute,
        length_of_position_vector,
        size_of_vertex_data
    );
    shaders.bind();

    QOpenGLFramebufferObject fbo(800, 600);

    fbo.bind();

    context.functions()->glClearColor(0.1f, 0.1f, 0.2f, 1.0f);
    context.functions()->glClear(GL_COLOR_BUFFER_BIT);
    context.functions()->glDrawArrays(GL_TRIANGLES, 0, 3);

    fbo.release();

    QImage image = fbo.toImage();
    image.save("rendered_image.png");

    return test_app.exec();
}