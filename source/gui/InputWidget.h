#pragma once

#include "image_creating/input/Input.h"

#include <QEventLoop>
#include <QLabel>
#include <QSlider>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

class InputWidget : public QWidget
{
  public:
    InputWidget();
    std::shared_ptr<ImageProcessorData> getInput();

  private:
    QPushButton* addButton(const QString& text);
    void         addTransform(const QString& text, Transformable* manipulation_object);
    QSlider*     addRotationSlider();
    QSlider*     addTranslationSlider(int position);
    void         addTransformsUI();

    QGridLayout            layout              = QGridLayout(this);
    constexpr static QSize WIDGET_SIZE         = {1280, 720};
    int                    buttons_number      = 0;
    int                    transforms_number   = 0;
    int                    transform_grid_size = 6;

    Input input;
};
