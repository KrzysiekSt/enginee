#include "TestWindow.h"

TestWindow::TestWindow(
    std::shared_ptr<ImageProcessorData> rendering_data, std::shared_ptr<ImageProcessor> given_image_processor
)
{
    setMinimumSize(QSize(1600, 900));
    setFlags(Qt::Window | Qt::MSWindowsFixedSizeDialogHint);
    rendering_data->rendering_size = this->size();
    image_processor                = given_image_processor;
    image_processor->assignData(rendering_data);

    //    if(given_image_processor->getApi() == ImageProcessor::RenderingApi::Vulkan)
    //    {
    //        auto instance_if_found = image_processor->tryToFindVulkanInstance();
    //        if(instance_if_found)
    //        {
    //            setVulkanInstance(instance_if_found);   // I think I don't need this ToDo cleaning
    //        }
    //        else { qDebug() << "Vulkan instance not in the pipeline !"; }
    //    }
}

void TestWindow::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    auto     rendered_image = image_processor->renderFrame();
    painter.drawImage(0, 0, rendered_image);
    painter.end();
}
void TestWindow::run()
{
    auto timer = new QTimer(this);   // do I have to create new one every time, i don't think so ToDo cleaning
    connect(timer, &QTimer::timeout, this, [this]() { this->update(); });
    timer->start(33);
}
