#pragma once

#include "image_creating/ImageProcessor.h"

#include <QBackingStore>
#include <QPainter>
#include <QRasterWindow>
#include <QResizeEvent>
#include <QTimer>
#include <memory>

class TestWindow : public QRasterWindow
{
    Q_OBJECT

  public:
    TestWindow(
        std::shared_ptr<ImageProcessorData> rendering_data, std::shared_ptr<ImageProcessor> given_image_processor
    );
    void run();

  protected:
    void paintEvent(QPaintEvent* event) override;

  private:
    std::shared_ptr<ImageProcessor> image_processor;
    QSurfaceFormat                  format;
};
