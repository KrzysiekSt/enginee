#include "InputWidget.h"

InputWidget::InputWidget()
{
    setWindowTitle("Input widget");
    resize(WIDGET_SIZE);

    QEventLoop wait_for_data;

    auto add_cube_button   = addButton("Add cube");
    auto add_model_button  = addButton("Add model");
    auto show_model_button = addButton("Show model");
    add_model_button->setDisabled(true);
    QObject::connect(add_cube_button, &QPushButton::clicked, &input, &Input::addCube);
    QObject::connect(add_cube_button, &QPushButton::clicked, this, &InputWidget::addTransformsUI);
    QObject::connect(
        add_cube_button,
        &QPushButton::clicked,
        this,
        [add_cube_button]() { add_cube_button->setDisabled(true); }
    );
    QObject::connect(add_model_button, &QPushButton::clicked, &input, &Input::addModelFromFile);
    QObject::connect(show_model_button, &QPushButton::clicked, &wait_for_data, &QEventLoop::quit);
    QObject::connect(
        show_model_button,
        &QPushButton::clicked,
        this,
        [add_cube_button]() { add_cube_button->setDisabled(true); }
    );

    setLayout(&layout);
    show();
    wait_for_data.exec();
}

QPushButton* InputWidget::addButton(const QString& text)
{
    auto button = new QPushButton(text, this);
    button->setObjectName(text);
    layout.addWidget(button, buttons_number++, 0);

    return button;
}

void InputWidget::addTransformsUI()
{
    addTransform("Camera", &getInput()->camera);
    addTransform("Model", getInput()->model);
    setLayout(&layout);
}

void InputWidget::addTransform(const QString& text, Transformable* manipulation_object)
{
    auto main_label        = new QLabel(text, this);
    auto translation_label = new QLabel("Translation", this);
    auto rotation_label    = new QLabel("Rotation", this);

    auto translation_label_axis_x = new QLabel("x", this);
    auto translation_label_axis_y = new QLabel("y", this);
    auto translation_label_axis_z = new QLabel("z", this);
    auto rotation_label_axis_x    = new QLabel("x", this);
    auto rotation_label_axis_y    = new QLabel("y", this);
    auto rotation_label_axis_z    = new QLabel("z", this);

    int position_x = manipulation_object->getPosition().x() / manipulation_object->getInputScale();
    int position_y = manipulation_object->getPosition().y() / manipulation_object->getInputScale();
    int position_z = manipulation_object->getPosition().z() / manipulation_object->getInputScale();

    auto translation_x_slider = addTranslationSlider(position_x);
    auto translation_y_slider = addTranslationSlider(position_y);
    auto translation_z_slider = addTranslationSlider(position_z);
    auto rotation_x_slider    = addRotationSlider();
    auto rotation_y_slider    = addRotationSlider();
    auto rotation_z_slider    = addRotationSlider();

    QObject::connect(translation_x_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setPositionX);
    QObject::connect(translation_y_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setPositionY);
    QObject::connect(translation_z_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setPositionZ);

    QObject::connect(rotation_x_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setRotationX);
    QObject::connect(rotation_y_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setRotationY);
    QObject::connect(rotation_z_slider, &QSlider::valueChanged, manipulation_object, &Transformable::setRotationZ);

    int start_row    = transforms_number++ * transform_grid_size;
    int start_column = 1;

    layout.addWidget(main_label, start_row, start_column, 1, 6);
    layout.addWidget(translation_label, start_row + 1, start_column, 1, 3);
    layout.addWidget(rotation_label, start_row + 1, start_column + 3, 1, 3);
    layout.addWidget(translation_label_axis_x, start_row + 2, start_column);
    layout.addWidget(translation_label_axis_y, start_row + 2, start_column + 1);
    layout.addWidget(translation_label_axis_z, start_row + 2, start_column + 2);
    layout.addWidget(rotation_label_axis_x, start_row + 2, start_column + 3);
    layout.addWidget(rotation_label_axis_y, start_row + 2, start_column + 4);
    layout.addWidget(rotation_label_axis_z, start_row + 2, start_column + 5);

    layout.addWidget(translation_x_slider, start_row + 3, start_column, 1, 3);
    layout.addWidget(translation_y_slider, start_row + 3, start_column + 1, 1, 3);
    layout.addWidget(translation_z_slider, start_row + 3, start_column + 2, 1, 3);
    layout.addWidget(rotation_x_slider, start_row + 3, start_column + 3, 1, 3);
    layout.addWidget(rotation_y_slider, start_row + 3, start_column + 4, 1, 3);
    layout.addWidget(rotation_z_slider, start_row + 3, start_column + 5, 1, 3);
}

QSlider* InputWidget::addRotationSlider()
{
    auto slider = new QSlider(Qt::Orientation::Vertical, this);

    slider->setTickInterval(1);
    slider->setMinimum(0);
    slider->setMaximum(359);

    return slider;
}

QSlider* InputWidget::addTranslationSlider(int position)
{
    auto slider = new QSlider(Qt::Orientation::Vertical, this);

    slider->setTickInterval(1);
    slider->setMinimum(-2000);
    slider->setMaximum(2000);
    slider->setValue(position);

    return slider;
}

std::shared_ptr<ImageProcessorData> InputWidget::getInput()
{
    return input.getInput();
}
