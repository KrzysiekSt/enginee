#pragma once

#include "NormalVector.h"
#include "PositionVector.h"

struct Vertex
{
    PositionVector position;
    NormalVector   normal;
};
