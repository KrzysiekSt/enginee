
#include "PerspectiveCamera.h"

#include <QTransform>

PerspectiveCamera::PerspectiveCamera(float vertical_FOV, float near_clip, float far_clip) :
    vertical_FOV(vertical_FOV), near_clip(near_clip), far_clip(far_clip)
{
    projection.perspective(vertical_FOV, (16.f / 9.f), near_clip, far_clip);
}

QMatrix4x4 PerspectiveCamera::getInPerspective(QMatrix4x4 to_project)
{
    to_project.perspective(vertical_FOV, (16.f / 9.f), near_clip, far_clip);
    return to_project;
}
