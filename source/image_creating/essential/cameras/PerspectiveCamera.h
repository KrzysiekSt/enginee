#pragma once

#include "image_creating/essential/Camera.h"

class PerspectiveCamera : public Camera
{
  public:
    PerspectiveCamera(float vertical_FOV, float near_clip, float far_clip);
    QMatrix4x4 getInPerspective(QMatrix4x4 to_project);

  private:
    float vertical_FOV;
    float near_clip;
    float far_clip;
};
