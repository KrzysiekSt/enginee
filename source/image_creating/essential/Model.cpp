#include "Model.h"

std::vector<QVector3D> Model::getVerticesPositions()
{
    std::vector<QVector3D> vertices_positions;
    if(vertices.empty()) return vertices_positions;
    vertices.reserve(vertices.size());

    for(auto& vertex: vertices)
    {
        vertices_positions.push_back(vertex.position);
    }

    return vertices_positions;
}

std::vector<std::array<uint32_t, 3>> Model::getFaces()
{
    return faces;
}