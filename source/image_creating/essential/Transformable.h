#pragma once

#include "QVulkanDeviceFunctions"

#include <QMatrix4x4>
#include <QObject>

class Transformable : public QObject
{
    Q_OBJECT

  public:
    Transformable();
    void        setRotation(const QQuaternion& given_rotation);
    void        setPosition(const QVector3D& given_position);
    QMatrix4x4  getTransform() { return transform; };
    QVector3D   getPosition() { return position; };
    QQuaternion getRotation() { return rotation; };
    float       getInputScale() { return ui_input_scale; };

    VkTransformMatrixKHR getVulkanTransform();

  public slots:
    void setPositionX(int value);
    void setPositionY(int value);
    void setPositionZ(int value);
    void setRotationX(int value);
    void setRotationY(int value);
    void setRotationZ(int value);

  private:
    void        recalculate();
    QMatrix4x4  transform;
    QVector3D   position       = {0, 0, 0};
    QQuaternion rotation       = QQuaternion::fromAxisAndAngle(QVector3D {0, 0, 1}, 0);
    float       ui_input_scale = 0.01;
};
