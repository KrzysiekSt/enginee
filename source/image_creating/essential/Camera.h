#pragma once

#include "Transformable.h"

#include <QMatrix4x4>

class Camera : public Transformable
{
  public:
    QMatrix4x4 getProjection() { return projection; };
    QMatrix4x4 getView() { return getTransform().inverted(); };

  protected:
    QMatrix4x4 projection;
};