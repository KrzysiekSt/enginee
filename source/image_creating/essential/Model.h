#pragma once
#include "Face.h"
#include "Transformable.h"
#include "Vertex.h"

#include <QArrayData>
#include <QLineF>
#include <QMatrix4x4>
#include <QVector>
#include <utility>

class Model : public Transformable
{
  public:
    Model() = default;
    Model(std::vector<Vertex> vertices, std::vector<std::array<uint32_t, 3>> faces) :
        vertices(std::move(vertices)), faces(std::move(faces))
    {}

    std::vector<PositionVector> getVerticesPositions();
    std::vector<Face>           getFaces();
    bool                        isValid() { return vertices.size() > 2; };

  private:
    std::vector<Vertex> vertices;
    std::vector<Face>   faces;
};
