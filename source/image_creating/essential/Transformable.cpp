#include "Transformable.h"

Transformable::Transformable()
{
    recalculate();
}

void Transformable::setPosition(const QVector3D& given_position)
{
    position = given_position;
    recalculate();
}

void Transformable::setRotation(const QQuaternion& given_rotation)
{
    rotation = given_rotation;
    recalculate();
}
void Transformable::recalculate()
{
    QMatrix4x4 position_matrix, rotation_matrix;
    position_matrix.translate(position);
    rotation_matrix.rotate(rotation);

    transform = position_matrix * rotation_matrix;
}
void Transformable::setPositionX(int value)
{
    position.setX(static_cast<float>(value) * ui_input_scale);
    recalculate();
}
void Transformable::setPositionY(int value)
{
    position.setY(static_cast<float>(value) * ui_input_scale);
    recalculate();
}
void Transformable::setPositionZ(int value)
{
    position.setZ(static_cast<float>(value) * ui_input_scale);
    recalculate();
}

void Transformable::setRotationX(int value)
{
    float x, y, z;
    rotation.getEulerAngles(&x, &y, &z);
    rotation = QQuaternion::fromEulerAngles(value, y, z);
    recalculate();
}
void Transformable::setRotationY(int value)
{
    float x, y, z;
    rotation.getEulerAngles(&x, &y, &z);
    rotation = QQuaternion::fromEulerAngles(x, value, z);
    recalculate();
}
void Transformable::setRotationZ(int value)
{
    float x, y, z;
    rotation.getEulerAngles(&x, &y, &z);
    rotation = QQuaternion::fromEulerAngles(x, y, value);
    recalculate();
}

VkTransformMatrixKHR Transformable::getVulkanTransform()
{
    VkTransformMatrixKHR out_matrix;

    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            out_matrix.matrix[i][j] = transform.constData()[i * 4 + j];
        }
    }

    // position ??
    out_matrix.matrix[0][3] = transform.constData()[12];
    out_matrix.matrix[1][3] = transform.constData()[13];
    out_matrix.matrix[2][3] = transform.constData()[14];

    return out_matrix;
}
