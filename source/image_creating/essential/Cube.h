#pragma once

#include "image_creating/essential/Model.h"

static inline Model cube = Model {

    std::vector<Vertex> {
                         Vertex {PositionVector {-0.5, -0.5, 0.5}},
                         Vertex {PositionVector {0.5, -0.5, 0.5}},
                         Vertex {PositionVector {-0.5, 0.5, 0.5}},
                         Vertex {PositionVector {0.5, 0.5, 0.5}},
                         Vertex {PositionVector {-0.5, -0.5, -0.5}},
                         Vertex {PositionVector {0.5, -0.5, -0.5}},
                         Vertex {PositionVector {-0.5, 0.5, -0.5}},
                         Vertex {PositionVector {0.5, 0.5, -0.5}}
    },

    std::vector<Face> {
                         {2, 6, 7},
                         {2, 3, 7},
                         {0, 4, 5},
                         {0, 1, 5},
                         {0, 2, 6},
                         {0, 4, 6},
                         {1, 3, 7},
                         {1, 5, 7},
                         {0, 2, 3},
                         {0, 1, 3},
                         {4, 6, 7},
                         {4, 5, 7}
    }
};