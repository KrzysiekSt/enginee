#pragma once

#include "ImageProcessorData.h"

#include <QString>
#include <QVulkanInstance>

class ImageProcessorStage
{
  public:
    virtual ~ImageProcessorStage() = default;
    virtual void                            init(std::shared_ptr<ImageProcessorData> given_data) { data = given_data; };
    virtual void                            process() = 0;
    virtual std::string                     getName() = 0;
    virtual std::optional<QVulkanInstance*> tryGetVulkanApiInstance() { return {}; }

  protected:
    std::shared_ptr<ImageProcessorData> data;
};
