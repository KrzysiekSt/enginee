#include "RasterizationImageProcessorFactory.h"

std::shared_ptr<ImageProcessor> RasterizationImageProcessorFactory::createOpenGLImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    stages.emplace_back(std::make_unique<OpenGLRenderingStage>());
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::OpenGL);
}

std::shared_ptr<ImageProcessor> RasterizationImageProcessorFactory::createVulkanImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    qCritical(
    ) << "This function is not yet implemented! - RayTracingImageProcessorFactory::createOpenGLImageProcessor";
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::Vulkan);
}
std::shared_ptr<ImageProcessor> RasterizationImageProcessorFactory::createCPUImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    qCritical(
    ) << "This function is not yet implemented! - RayTracingImageProcessorFactory::createOpenGLImageProcessor";
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::None);
}
