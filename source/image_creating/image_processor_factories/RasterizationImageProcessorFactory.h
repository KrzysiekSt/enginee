#pragma once
#include "image_creating/ImageProcessorFactory.h"
#include "image_creating/Image_processor_stages/rendering/OpenGLRenderingStage.h"

class RasterizationImageProcessorFactory : public ImageProcessorFactory
{
  public:
    std::shared_ptr<ImageProcessor> createOpenGLImageProcessor() const override;
    std::shared_ptr<ImageProcessor> createVulkanImageProcessor() const override;
    std::shared_ptr<ImageProcessor> createCPUImageProcessor() const override;
};