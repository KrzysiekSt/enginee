#include "RayTracingImageProcessorFactory.h"

std::shared_ptr<ImageProcessor> RayTracingImageProcessorFactory::createOpenGLImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    qCritical(
    ) << "This function is not yet implemented! - RayTracingImageProcessorFactory::createOpenGLImageProcessor";
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::OpenGL);
}

std::shared_ptr<ImageProcessor> RayTracingImageProcessorFactory::createVulkanImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    stages.emplace_back(std::make_unique<VulkanRTRenderingStage>());
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::Vulkan);
}

std::shared_ptr<ImageProcessor> RayTracingImageProcessorFactory::createCPUImageProcessor() const
{
    std::vector<std::unique_ptr<ImageProcessorStage>> stages = {};
    stages.emplace_back(std::make_unique<CPUBasedRayTracingSceneStage>());
    return std::make_shared<ImageProcessor>(std::move(stages), ImageProcessor::RenderingApi::None);
}
