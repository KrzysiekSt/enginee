#include "QVulkanLayer"
#include "VulkanRTRenderingStage.h"

VulkanRTRenderingStage::VulkanRTRenderingStage()
{
    graphics_engine = VulakanEngine(VulakanEngine::Pipeline::RayTracing);
}

void VulkanRTRenderingStage::init(std::shared_ptr<ImageProcessorData> given_data)
{
    ImageProcessorStage::init(given_data);

    given_data->model;

    graphics_engine.addObject(given_data->cude)
}

void VulkanRTRenderingStage::process() {}

std::string VulkanRTRenderingStage::getName()
{
    return "VulkanRTRenderingStage";
}

VulkanRTRenderingStage::~VulkanRTRenderingStage() {}
