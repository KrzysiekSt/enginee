#pragma once

#include "image_creating/ImageProcessorStage.h"
#include "image_creating/Image_processor_stages/rendering/vulkan_abstraction/Device.h"
#include "image_creating/Image_processor_stages/rendering/vulkan_abstraction/VulkanRTRenderingFunctions.h"

#include <QVulkanDeviceFunctions>
#include <QVulkanFunctions>
#include <QVulkanInstance>
#include <vector>

class VulkanRTRenderingStage : public ImageProcessorStage
{
  public:
    VulkanRTRenderingStage();
    ~VulkanRTRenderingStage() override;
    void                            init(std::shared_ptr<ImageProcessorData> given_data) override;
    void                            process() override;
    std::string                     getName() override;
    std::optional<QVulkanInstance*> tryGetVulkanApiInstance() override { return &vulkan_context; }

  private:
    Device                      device;
    VulkanRTRenderingFunctions* vulkan_worker;
    uint32_t                    queue_family_index;

    const std::string GPU_NAME = "AMD Radeon RX 7900 XTX";

    const std::vector<std::string> REQUIRED_DEVICE_EXTENSIONS = {
        "VK_KHR_ray_tracing_pipeline",
        "VK_KHR_acceleration_structure",
        "VK_EXT_descriptor_indexing",
        "VK_KHR_maintenance3",
        "VK_KHR_buffer_device_address",
        "VK_KHR_deferred_host_operations",
    };
    QVulkanInstance vulkan_context;

    float frames = 0;
};
