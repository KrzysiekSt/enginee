#pragma once

#include "image_creating/ImageProcessorStage.h"

#include <QDebug>
#include <QGeometry>
#include <QOffscreenSurface>
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <memory>

class OpenGLRenderingStage : public ImageProcessorStage
{
  public:
    OpenGLRenderingStage();
    ~OpenGLRenderingStage() override;
    void        init(std::shared_ptr<ImageProcessorData> given_data) override;
    void        process() override;
    std::string getName() override;

  private:
    float                                     frames = 0;
    QOpenGLShaderProgram                      shaders;
    QOpenGLBuffer                             vertex_buffer;
    QOpenGLBuffer                             index_buffer;
    QOpenGLFramebufferObjectFormat            frame_buffer_object_format;
    QSurfaceFormat                            surface_format;
    QOffscreenSurface                         surface             = QOffscreenSurface();
    QOpenGLContext                            opengl_context      = QOpenGLContext();
    std::unique_ptr<QOpenGLFramebufferObject> frame_buffer_object = nullptr;

    unsigned int      buffer;
    const int         vertex_attribute_index       = 0;
    const int         length_of_position_vector    = 3;
    const int         offset_to_attribute          = 0;
    const std::string projection_view_uniform_name = "projection_view";
    const std::string model_uniform_name           = "model";
    const std::string model_uniform_color_name     = "in_color";
    const QVector4D   model_color                  = QVector4D {0.7, 1, 0.7, 1};
    const QVector4D   edge_color                   = QVector4D {0.1, 0.1, 0.1, 1};

    void setupContext();
    void compileShaders();
    void createBuffers();
    void renderModel();
};
