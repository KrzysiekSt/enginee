#include "VulkanRTRenderingStage.h"

#include "QVulkanLayer"

// shaders
#include "image_creating/shaders/headers/hit.h"
#include "image_creating/shaders/headers/miss.h"
#include "image_creating/shaders/headers/ray.h"
#include "image_creating/shaders/headers/shadow.h"

VulkanRTRenderingStage::VulkanRTRenderingStage()
{
    qDebug() << "Vulkan version =" << vulkan_context.supportedApiVersion().toString();
    qDebug() << "vulkan supported layers: " << vulkan_context.supportedLayers();

    vulkan_worker = VulkanRTRenderingFunctions::getInstance(vulkan_context);
    bool success  = vulkan_worker->createWithLayers({"VK_LAYER_KHRONOS_validation"});
    if(not success)
    {
        qCritical() << "createWithLayers";
        return;
    }

    vulkan_worker->checkAvailableExtensions({});

    auto found_device = vulkan_worker->findDevice(GPU_NAME);
    if(found_device.has_value())
    {
        device.physical_device = found_device.value();
    }
    else
    {
        qCritical() << "findDevice";
        return;
    }

    auto found_queue_family_index = vulkan_worker->getQueueFamilyIndex(device, {VK_QUEUE_GRAPHICS_BIT});
    if(found_queue_family_index.has_value())
    {
        queue_family_index = found_queue_family_index.value();
    }
    else
    {
        qCritical() << "getQueueFamilyIndex";
        return;
    }

    auto created_device = vulkan_worker->createLogicalDevice(device, queue_family_index, REQUIRED_DEVICE_EXTENSIONS);
    if(created_device.has_value()) device.logical_device = created_device.value();
    else
    {
        qCritical() << "createLogicalDevice";
        return;
    }
}

void VulkanRTRenderingStage::init(std::shared_ptr<ImageProcessorData> given_data)
{
    ImageProcessorStage::init(given_data);
}

void VulkanRTRenderingStage::process()
{
    VkResult result;

    // =========================================================================
    // Vulkan Instance

    std::vector<VkValidationFeatureEnableEXT> validationFeatureEnableList = {
        // VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
        VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT,
        VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT
    };

    VkDebugUtilsMessageSeverityFlagsEXT debugUtilsMessageSeverityFlagBits = (
        // VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
    );

    VkDebugUtilsMessageTypeFlagsEXT debugUtilsMessageTypeFlagBits = (
        // VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
    );

    VkValidationFeaturesEXT validationFeatures = {
        .sType                          = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT,
        .pNext                          = nullptr,
        .enabledValidationFeatureCount  = (uint32_t)validationFeatureEnableList.size(),
        .pEnabledValidationFeatures     = validationFeatureEnableList.data(),
        .disabledValidationFeatureCount = 0,
        .pDisabledValidationFeatures    = nullptr
    };

    VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCreateInfo = {
        .sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .pNext           = &validationFeatures,
        .flags           = 0,
        .messageSeverity = debugUtilsMessageSeverityFlagBits,
        .messageType     = debugUtilsMessageTypeFlagBits,
        .pUserData       = nullptr
    };

    VkApplicationInfo applicationInfo = {
        .sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext              = nullptr,
        .pApplicationName   = "Enginee",
        .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
        .pEngineName        = "",
        .engineVersion      = VK_MAKE_VERSION(0, 0, 1),
        .apiVersion         = VK_API_VERSION_1_3
    };

    std::vector<const char*> instanceLayerList = {};

    std::vector<const char*> instanceExtensionList = {
        VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
        "VK_KHR_get_physical_device_properties2"
    };

    VkInstanceCreateInfo instanceCreateInfo = {
        .sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext                   = &debugUtilsMessengerCreateInfo,
        .flags                   = 0,
        .pApplicationInfo        = &applicationInfo,
        .enabledLayerCount       = (uint32_t)instanceLayerList.size(),
        .ppEnabledLayerNames     = instanceLayerList.data(),
        .enabledExtensionCount   = (uint32_t)instanceExtensionList.size(),
        .ppEnabledExtensionNames = instanceExtensionList.data(),
    };

    // =========================================================================
    // Submission Queue

    VkQueue queueHandle = VK_NULL_HANDLE;
    vulkan_context.deviceFunctions(device)->vkGetDeviceQueue(device, queue_family_index, 0, &queueHandle);

    // =========================================================================
    // Device Pointer Functions

    PFN_vkGetBufferDeviceAddressKHR pvkGetBufferDeviceAddressKHR = reinterpret_cast<PFN_vkGetBufferDeviceAddressKHR>(
        vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkGetBufferDeviceAddressKHR")
    );

    PFN_vkCreateRayTracingPipelinesKHR pvkCreateRayTracingPipelinesKHR =
        reinterpret_cast<PFN_vkCreateRayTracingPipelinesKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkCreateRayTracingPipelinesKHR")
        );

    PFN_vkGetAccelerationStructureBuildSizesKHR pvkGetAccelerationStructureBuildSizesKHR =
        reinterpret_cast<PFN_vkGetAccelerationStructureBuildSizesKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkGetAccelerationStructureBuildSizesKHR")
        );

    PFN_vkCreateAccelerationStructureKHR pvkCreateAccelerationStructureKHR =
        reinterpret_cast<PFN_vkCreateAccelerationStructureKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkCreateAccelerationStructureKHR")
        );

    PFN_vkDestroyAccelerationStructureKHR pvkDestroyAccelerationStructureKHR =
        reinterpret_cast<PFN_vkDestroyAccelerationStructureKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkDestroyAccelerationStructureKHR")
        );

    PFN_vkGetAccelerationStructureDeviceAddressKHR pvkGetAccelerationStructureDeviceAddressKHR =
        reinterpret_cast<PFN_vkGetAccelerationStructureDeviceAddressKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkGetAccelerationStructureDeviceAddressKHR")
        );

    PFN_vkCmdBuildAccelerationStructuresKHR pvkCmdBuildAccelerationStructuresKHR =
        reinterpret_cast<PFN_vkCmdBuildAccelerationStructuresKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkCmdBuildAccelerationStructuresKHR")
        );

    PFN_vkGetRayTracingShaderGroupHandlesKHR pvkGetRayTracingShaderGroupHandlesKHR =
        reinterpret_cast<PFN_vkGetRayTracingShaderGroupHandlesKHR>(
            vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkGetRayTracingShaderGroupHandlesKHR")
        );

    PFN_vkCmdTraceRaysKHR pvkCmdTraceRaysKHR = reinterpret_cast<PFN_vkCmdTraceRaysKHR>(
        vulkan_context.functions()->vkGetDeviceProcAddr(device, "vkCmdTraceRaysKHR")
    );

    VkMemoryAllocateFlagsInfo memoryAllocateFlagsInfo = {
        .sType      = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
        .pNext      = nullptr,
        .flags      = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT,
        .deviceMask = 0
    };

    // =========================================================================
    // Command Pool

    VkCommandPoolCreateInfo commandPoolCreateInfo = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext            = nullptr,
        .flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = queue_family_index
    };

    VkCommandPool commandPoolHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)
                 ->vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPoolHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateCommandPool";
    }

    // =========================================================================
    // Command Buffers

    VkCommandBufferAllocateInfo commandBufferAllocateInfo = {
        .sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext              = nullptr,
        .commandPool        = commandPoolHandle,
        .level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 16
    };

    std::vector<VkCommandBuffer> commandBufferHandleList = std::vector<VkCommandBuffer>(16, VK_NULL_HANDLE);

    result = vulkan_context.deviceFunctions(device)
                 ->vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, commandBufferHandleList.data());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateCommandBuffers";
    }

    // =========================================================================
    // Descriptor Pool

    std::vector<VkDescriptorPoolSize> descriptorPoolSizeList = {
        {.type = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, .descriptorCount = 1},
        {            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, .descriptorCount = 1},
        {            .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 4},
        {             .type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = 1}
    };

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {
        .sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext         = nullptr,
        .flags         = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
        .maxSets       = 2,
        .poolSizeCount = (uint32_t)descriptorPoolSizeList.size(),
        .pPoolSizes    = descriptorPoolSizeList.data()
    };

    VkDescriptorPool descriptorPoolHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)
                 ->vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, nullptr, &descriptorPoolHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateDescriptorPool";
    }

    // =========================================================================
    // Descriptor Set Layout

    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindingList = {
        {.binding            = 0,
         .descriptorType     = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr},
        {.binding            = 1,
         .descriptorType     = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr},
        {.binding            = 2,
         .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr},
        {.binding            = 3,
         .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr},
        {.binding            = 4,
         .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_RAYGEN_BIT_KHR,
         .pImmutableSamplers = nullptr}
    };

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {
        .sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext        = nullptr,
        .flags        = 0,
        .bindingCount = (uint32_t)descriptorSetLayoutBindingList.size(),
        .pBindings    = descriptorSetLayoutBindingList.data()
    };

    VkDescriptorSetLayout descriptorSetLayoutHandle = VK_NULL_HANDLE;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayoutHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateDescriptorSetLayout";
    }

    // =========================================================================
    // Material Descriptor Set Layout

    std::vector<VkDescriptorSetLayoutBinding> materialDescriptorSetLayoutBindingList = {
        {.binding            = 0,
         .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr},
        {.binding            = 1,
         .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .descriptorCount    = 1,
         .stageFlags         = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .pImmutableSamplers = nullptr}
    };

    VkDescriptorSetLayoutCreateInfo materialDescriptorSetLayoutCreateInfo = {
        .sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext        = nullptr,
        .flags        = 0,
        .bindingCount = (uint32_t)materialDescriptorSetLayoutBindingList.size(),
        .pBindings    = materialDescriptorSetLayoutBindingList.data()
    };

    VkDescriptorSetLayout materialDescriptorSetLayoutHandle = VK_NULL_HANDLE;
    result = vulkan_context.deviceFunctions(device)->vkCreateDescriptorSetLayout(
        device,
        &materialDescriptorSetLayoutCreateInfo,
        nullptr,
        &materialDescriptorSetLayoutHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateDescriptorSetLayout";
    }

    // =========================================================================
    // Allocate Descriptor Sets

    std::vector<VkDescriptorSetLayout> descriptorSetLayoutHandleList = {
        descriptorSetLayoutHandle,
        materialDescriptorSetLayoutHandle
    };

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo = {
        .sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext              = nullptr,
        .descriptorPool     = descriptorPoolHandle,
        .descriptorSetCount = (uint32_t)descriptorSetLayoutHandleList.size(),
        .pSetLayouts        = descriptorSetLayoutHandleList.data()
    };

    std::vector<VkDescriptorSet> descriptorSetHandleList = std::vector<VkDescriptorSet>(2, VK_NULL_HANDLE);

    result = vulkan_context.deviceFunctions(device)
                 ->vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, descriptorSetHandleList.data());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateDescriptorSets";
    }

    // =========================================================================
    // Pipeline Layout

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
        .sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext                  = nullptr,
        .flags                  = 0,
        .setLayoutCount         = (uint32_t)descriptorSetLayoutHandleList.size(),
        .pSetLayouts            = descriptorSetLayoutHandleList.data(),
        .pushConstantRangeCount = 0,
        .pPushConstantRanges    = nullptr
    };

    VkPipelineLayout pipelineLayoutHandle = VK_NULL_HANDLE;
    result                                = vulkan_context.deviceFunctions(device)
                 ->vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayoutHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreatePipelineLayout";
    }

    // =========================================================================
    // Ray Closest Hit Shader Module

    VkShaderModuleCreateInfo rayClosestHitShaderModuleCreateInfo = {
        .sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext    = nullptr,
        .flags    = 0,
        .codeSize = hit_shader_len,
        .pCode    = hit_shader
    };

    VkShaderModule rayClosestHitShaderModuleHandle = VK_NULL_HANDLE;
    result                                         = vulkan_context.deviceFunctions(device)->vkCreateShaderModule(
        device,
        &rayClosestHitShaderModuleCreateInfo,
        nullptr,
        &rayClosestHitShaderModuleHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateShaderModule";
    }

    // =========================================================================
    // Ray Generate Shader Module

    VkShaderModuleCreateInfo rayGenerateShaderModuleCreateInfo = {
        .sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext    = nullptr,
        .flags    = 0,
        .codeSize = ray_shader_len,
        .pCode    = ray_shader
    };

    VkShaderModule rayGenerateShaderModuleHandle = VK_NULL_HANDLE;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkCreateShaderModule(device, &rayGenerateShaderModuleCreateInfo, nullptr, &rayGenerateShaderModuleHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateShaderModule";
    }

    // =========================================================================
    // Ray Miss Shader Module

    VkShaderModuleCreateInfo rayMissShaderModuleCreateInfo = {
        .sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext    = nullptr,
        .flags    = 0,
        .codeSize = miss_shader_len,
        .pCode    = miss_shader
    };

    VkShaderModule rayMissShaderModuleHandle = VK_NULL_HANDLE;
    result                                   = vulkan_context.deviceFunctions(device)
                 ->vkCreateShaderModule(device, &rayMissShaderModuleCreateInfo, nullptr, &rayMissShaderModuleHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateShaderModule";
    }

    // =========================================================================
    // Ray Miss Shader Module (Shadow)

    VkShaderModuleCreateInfo rayMissShadowShaderModuleCreateInfo = {
        .sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext    = nullptr,
        .flags    = 0,
        .codeSize = shadow_shader_len,
        .pCode    = shadow_shader
    };

    VkShaderModule rayMissShadowShaderModuleHandle = VK_NULL_HANDLE;
    result                                         = vulkan_context.deviceFunctions(device)->vkCreateShaderModule(
        device,
        &rayMissShadowShaderModuleCreateInfo,
        nullptr,
        &rayMissShadowShaderModuleHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateShaderModule";
    }

    // =========================================================================
    // Ray Tracing Pipeline

    std::vector<VkPipelineShaderStageCreateInfo> pipelineShaderStageCreateInfoList = {
        {.sType               = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
         .pNext               = nullptr,
         .flags               = 0,
         .stage               = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR,
         .module              = rayClosestHitShaderModuleHandle,
         .pName               = "main",
         .pSpecializationInfo = nullptr},
        {.sType               = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
         .pNext               = nullptr,
         .flags               = 0,
         .stage               = VK_SHADER_STAGE_RAYGEN_BIT_KHR,
         .module              = rayGenerateShaderModuleHandle,
         .pName               = "main",
         .pSpecializationInfo = nullptr},
        {.sType               = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
         .pNext               = nullptr,
         .flags               = 0,
         .stage               = VK_SHADER_STAGE_MISS_BIT_KHR,
         .module              = rayMissShaderModuleHandle,
         .pName               = "main",
         .pSpecializationInfo = nullptr},
        {.sType               = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
         .pNext               = nullptr,
         .flags               = 0,
         .stage               = VK_SHADER_STAGE_MISS_BIT_KHR,
         .module              = rayMissShadowShaderModuleHandle,
         .pName               = "main",
         .pSpecializationInfo = nullptr}
    };

    std::vector<VkRayTracingShaderGroupCreateInfoKHR> rayTracingShaderGroupCreateInfoList = {
        {.sType                           = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR,
         .pNext                           = nullptr,
         .type                            = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR,
         .generalShader                   = VK_SHADER_UNUSED_KHR,
         .closestHitShader                = 0,
         .anyHitShader                    = VK_SHADER_UNUSED_KHR,
         .intersectionShader              = VK_SHADER_UNUSED_KHR,
         .pShaderGroupCaptureReplayHandle = nullptr},
        {.sType                           = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR,
         .pNext                           = nullptr,
         .type                            = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR,
         .generalShader                   = 1,
         .closestHitShader                = VK_SHADER_UNUSED_KHR,
         .anyHitShader                    = VK_SHADER_UNUSED_KHR,
         .intersectionShader              = VK_SHADER_UNUSED_KHR,
         .pShaderGroupCaptureReplayHandle = nullptr},
        {.sType                           = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR,
         .pNext                           = nullptr,
         .type                            = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR,
         .generalShader                   = 2,
         .closestHitShader                = VK_SHADER_UNUSED_KHR,
         .anyHitShader                    = VK_SHADER_UNUSED_KHR,
         .intersectionShader              = VK_SHADER_UNUSED_KHR,
         .pShaderGroupCaptureReplayHandle = nullptr},
        {.sType                           = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR,
         .pNext                           = nullptr,
         .type                            = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR,
         .generalShader                   = 3,
         .closestHitShader                = VK_SHADER_UNUSED_KHR,
         .anyHitShader                    = VK_SHADER_UNUSED_KHR,
         .intersectionShader              = VK_SHADER_UNUSED_KHR,
         .pShaderGroupCaptureReplayHandle = nullptr}
    };

    VkRayTracingPipelineCreateInfoKHR rayTracingPipelineCreateInfo = {
        .sType                        = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR,
        .pNext                        = nullptr,
        .flags                        = 0,
        .stageCount                   = 4,
        .pStages                      = pipelineShaderStageCreateInfoList.data(),
        .groupCount                   = 4,
        .pGroups                      = rayTracingShaderGroupCreateInfoList.data(),
        .maxPipelineRayRecursionDepth = 1,
        .pLibraryInfo                 = nullptr,
        .pLibraryInterface            = nullptr,
        .pDynamicState                = nullptr,
        .layout                       = pipelineLayoutHandle,
        .basePipelineHandle           = VK_NULL_HANDLE,
        .basePipelineIndex            = 0
    };

    VkPipeline rayTracingPipelineHandle = VK_NULL_HANDLE;

    result = pvkCreateRayTracingPipelinesKHR(
        device,
        VK_NULL_HANDLE,
        VK_NULL_HANDLE,
        1,
        &rayTracingPipelineCreateInfo,
        nullptr,
        &rayTracingPipelineHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateRayTracingPipelinesKHR";
    }

    // =========================================================================
    // Model

    auto verticesPosition = data->model->getVerticesPositions();
    auto faces            = data->model->getFaces();

    uint32_t              primitiveCount = static_cast<uint32_t>(faces.size());
    std::vector<uint32_t> indexList;

    for(const auto& face: faces)
    {
        indexList.insert(indexList.end(), face.begin(), face.end());
    }

    // =========================================================================
    // Vertex Buffer

    VkBufferCreateInfo vertexBufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size  = sizeof(float) * verticesPosition.size() * 3,
        .usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                 VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                 VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer vertexBufferHandle = VK_NULL_HANDLE;
    result                      = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &vertexBufferCreateInfo, nullptr, &vertexBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements vertexMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, vertexBufferHandle, &vertexMemoryRequirements);

    VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
    vulkan_context.functions()->vkGetPhysicalDeviceMemoryProperties(device, &physicalDeviceMemoryProperties);

    uint32_t vertexMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((vertexMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            vertexMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo vertexMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = vertexMemoryRequirements.size,
        .memoryTypeIndex = vertexMemoryTypeIndex
    };

    VkDeviceMemory vertexDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                  = vulkan_context.deviceFunctions(device)
                 ->vkAllocateMemory(device, &vertexMemoryAllocateInfo, nullptr, &vertexDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, vertexBufferHandle, vertexDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostVertexMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        vertexDeviceMemoryHandle,
        0,
        sizeof(float) * verticesPosition.size() * 3,
        0,
        &hostVertexMemoryBuffer
    );

    memcpy(hostVertexMemoryBuffer, verticesPosition.data(), sizeof(float) * verticesPosition.size() * 3);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, vertexDeviceMemoryHandle);

    VkBufferDeviceAddressInfo vertexBufferDeviceAddressInfo =
        {.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO, .pNext = nullptr, .buffer = vertexBufferHandle};

    VkDeviceAddress vertexBufferDeviceAddress = pvkGetBufferDeviceAddressKHR(device, &vertexBufferDeviceAddressInfo);

    // =========================================================================
    // Index Buffer

    VkBufferCreateInfo indexBufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size  = sizeof(uint32_t) * indexList.size(),
        .usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                 VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                 VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer indexBufferHandle = VK_NULL_HANDLE;
    result                     = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &indexBufferCreateInfo, nullptr, &indexBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements indexMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, indexBufferHandle, &indexMemoryRequirements);

    uint32_t indexMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((indexMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            indexMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo indexMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = indexMemoryRequirements.size,
        .memoryTypeIndex = indexMemoryTypeIndex
    };

    VkDeviceMemory indexDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                 = vulkan_context.deviceFunctions(device)
                 ->vkAllocateMemory(device, &indexMemoryAllocateInfo, nullptr, &indexDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, indexBufferHandle, indexDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostIndexMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        indexDeviceMemoryHandle,
        0,
        sizeof(uint32_t) * indexList.size(),
        0,
        &hostIndexMemoryBuffer
    );

    memcpy(hostIndexMemoryBuffer, indexList.data(), sizeof(uint32_t) * indexList.size());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, indexDeviceMemoryHandle);

    VkBufferDeviceAddressInfo indexBufferDeviceAddressInfo =
        {.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO, .pNext = nullptr, .buffer = indexBufferHandle};

    VkDeviceAddress indexBufferDeviceAddress = pvkGetBufferDeviceAddressKHR(device, &indexBufferDeviceAddressInfo);

    // =========================================================================
    // Bottom Level Acceleration Structure

    VkAccelerationStructureGeometryDataKHR bottomLevelAccelerationStructureGeometryData = {
        .triangles =
            {.sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
                        .pNext         = nullptr,
                        .vertexFormat  = VK_FORMAT_R32G32B32_SFLOAT,
                        .vertexData    = {.deviceAddress = vertexBufferDeviceAddress},
                        .vertexStride  = sizeof(float) * 3,
                        .maxVertex     = (uint32_t)verticesPosition.size(),
                        .indexType     = VK_INDEX_TYPE_UINT32,
                        .indexData     = {.deviceAddress = indexBufferDeviceAddress},
                        .transformData = {.deviceAddress = 0}}
    };

    VkAccelerationStructureGeometryKHR bottomLevelAccelerationStructureGeometry = {
        .sType        = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
        .pNext        = nullptr,
        .geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR,
        .geometry     = bottomLevelAccelerationStructureGeometryData,
        .flags        = VK_GEOMETRY_OPAQUE_BIT_KHR
    };

    VkAccelerationStructureBuildGeometryInfoKHR bottomLevelAccelerationStructureBuildGeometryInfo = {
        .sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
        .pNext                    = nullptr,
        .type                     = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
        .flags                    = 0,
        .mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
        .srcAccelerationStructure = VK_NULL_HANDLE,
        .dstAccelerationStructure = VK_NULL_HANDLE,
        .geometryCount            = 1,
        .pGeometries              = &bottomLevelAccelerationStructureGeometry,
        .ppGeometries             = nullptr,
        .scratchData              = {.deviceAddress = 0}
    };

    VkAccelerationStructureBuildSizesInfoKHR bottomLevelAccelerationStructureBuildSizesInfo = {
        .sType                     = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
        .pNext                     = nullptr,
        .accelerationStructureSize = 0,
        .updateScratchSize         = 0,
        .buildScratchSize          = 0
    };

    std::vector<uint32_t> bottomLevelMaxPrimitiveCountList = {primitiveCount};

    pvkGetAccelerationStructureBuildSizesKHR(
        device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &bottomLevelAccelerationStructureBuildGeometryInfo,
        bottomLevelMaxPrimitiveCountList.data(),
        &bottomLevelAccelerationStructureBuildSizesInfo
    );

    VkBufferCreateInfo bottomLevelAccelerationStructureBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = bottomLevelAccelerationStructureBuildSizesInfo.accelerationStructureSize,
        .usage                 = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer bottomLevelAccelerationStructureBufferHandle = VK_NULL_HANDLE;
    result                                                = vulkan_context.deviceFunctions(device)->vkCreateBuffer(
        device,
        &bottomLevelAccelerationStructureBufferCreateInfo,
        nullptr,
        &bottomLevelAccelerationStructureBufferHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements bottomLevelAccelerationStructureMemoryRequirements;
    vulkan_context.deviceFunctions(device)->vkGetBufferMemoryRequirements(
        device,
        bottomLevelAccelerationStructureBufferHandle,
        &bottomLevelAccelerationStructureMemoryRequirements
    );

    uint32_t bottomLevelAccelerationStructureMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((bottomLevelAccelerationStructureMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ==
               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            bottomLevelAccelerationStructureMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo bottomLevelAccelerationStructureMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = nullptr,
        .allocationSize  = bottomLevelAccelerationStructureMemoryRequirements.size,
        .memoryTypeIndex = bottomLevelAccelerationStructureMemoryTypeIndex
    };

    VkDeviceMemory bottomLevelAccelerationStructureDeviceMemoryHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &bottomLevelAccelerationStructureMemoryAllocateInfo,
        nullptr,
        &bottomLevelAccelerationStructureDeviceMemoryHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)->vkBindBufferMemory(
        device,
        bottomLevelAccelerationStructureBufferHandle,
        bottomLevelAccelerationStructureDeviceMemoryHandle,
        0
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    VkAccelerationStructureCreateInfoKHR bottomLevelAccelerationStructureCreateInfo = {
        .sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR,
        .pNext         = nullptr,
        .createFlags   = 0,
        .buffer        = bottomLevelAccelerationStructureBufferHandle,
        .offset        = 0,
        .size          = bottomLevelAccelerationStructureBuildSizesInfo.accelerationStructureSize,
        .type          = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR,
        .deviceAddress = 0
    };

    VkAccelerationStructureKHR bottomLevelAccelerationStructureHandle = VK_NULL_HANDLE;

    result = pvkCreateAccelerationStructureKHR(
        device,
        &bottomLevelAccelerationStructureCreateInfo,
        nullptr,
        &bottomLevelAccelerationStructureHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateAccelerationStructureKHR";
    }

    // =========================================================================
    // Build Bottom Level Acceleration Structure

    VkAccelerationStructureDeviceAddressInfoKHR bottomLevelAccelerationStructureDeviceAddressInfo = {
        .sType                 = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR,
        .pNext                 = nullptr,
        .accelerationStructure = bottomLevelAccelerationStructureHandle
    };

    VkDeviceAddress bottomLevelAccelerationStructureDeviceAddress =
        pvkGetAccelerationStructureDeviceAddressKHR(device, &bottomLevelAccelerationStructureDeviceAddressInfo);

    VkBufferCreateInfo bottomLevelAccelerationStructureScratchBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = bottomLevelAccelerationStructureBuildSizesInfo.buildScratchSize,
        .usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer bottomLevelAccelerationStructureScratchBufferHandle = VK_NULL_HANDLE;
    result = vulkan_context.deviceFunctions(device)->vkCreateBuffer(
        device,
        &bottomLevelAccelerationStructureScratchBufferCreateInfo,
        nullptr,
        &bottomLevelAccelerationStructureScratchBufferHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements bottomLevelAccelerationStructureScratchMemoryRequirements;
    vulkan_context.deviceFunctions(device)->vkGetBufferMemoryRequirements(
        device,
        bottomLevelAccelerationStructureScratchBufferHandle,
        &bottomLevelAccelerationStructureScratchMemoryRequirements
    );

    uint32_t bottomLevelAccelerationStructureScratchMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((bottomLevelAccelerationStructureScratchMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ==
               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            bottomLevelAccelerationStructureScratchMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo bottomLevelAccelerationStructureScratchMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = bottomLevelAccelerationStructureScratchMemoryRequirements.size,
        .memoryTypeIndex = bottomLevelAccelerationStructureScratchMemoryTypeIndex
    };

    VkDeviceMemory bottomLevelAccelerationStructureDeviceScratchMemoryHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &bottomLevelAccelerationStructureScratchMemoryAllocateInfo,
        nullptr,
        &bottomLevelAccelerationStructureDeviceScratchMemoryHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)->vkBindBufferMemory(
        device,
        bottomLevelAccelerationStructureScratchBufferHandle,
        bottomLevelAccelerationStructureDeviceScratchMemoryHandle,
        0
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    VkBufferDeviceAddressInfo bottomLevelAccelerationStructureScratchBufferDeviceAddressInfo = {
        .sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext  = nullptr,
        .buffer = bottomLevelAccelerationStructureScratchBufferHandle
    };

    VkDeviceAddress bottomLevelAccelerationStructureScratchBufferDeviceAddress =
        pvkGetBufferDeviceAddressKHR(device, &bottomLevelAccelerationStructureScratchBufferDeviceAddressInfo);

    bottomLevelAccelerationStructureBuildGeometryInfo.dstAccelerationStructure = bottomLevelAccelerationStructureHandle;

    bottomLevelAccelerationStructureBuildGeometryInfo.scratchData = {
        .deviceAddress = bottomLevelAccelerationStructureScratchBufferDeviceAddress
    };

    VkAccelerationStructureBuildRangeInfoKHR bottomLevelAccelerationStructureBuildRangeInfo =
        {.primitiveCount = primitiveCount, .primitiveOffset = 0, .firstVertex = 0, .transformOffset = 0};

    const VkAccelerationStructureBuildRangeInfoKHR* bottomLevelAccelerationStructureBuildRangeInfos =
        &bottomLevelAccelerationStructureBuildRangeInfo;

    VkCommandBufferBeginInfo bottomLevelCommandBufferBeginInfo = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext            = nullptr,
        .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr
    };

    result = vulkan_context.deviceFunctions(device)->vkBeginCommandBuffer(
        commandBufferHandleList.back(),
        &bottomLevelCommandBufferBeginInfo
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBeginCommandBuffer";
    }

    pvkCmdBuildAccelerationStructuresKHR(
        commandBufferHandleList.back(),
        1,
        &bottomLevelAccelerationStructureBuildGeometryInfo,
        &bottomLevelAccelerationStructureBuildRangeInfos
    );

    result = vulkan_context.deviceFunctions(device)->vkEndCommandBuffer(commandBufferHandleList.back());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkEndCommandBuffer";
    }

    VkSubmitInfo bottomLevelAccelerationStructureBuildSubmitInfo = {
        .sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext                = nullptr,
        .waitSemaphoreCount   = 0,
        .pWaitSemaphores      = nullptr,
        .pWaitDstStageMask    = nullptr,
        .commandBufferCount   = 1,
        .pCommandBuffers      = &commandBufferHandleList.back(),
        .signalSemaphoreCount = 0,
        .pSignalSemaphores    = nullptr
    };

    VkFenceCreateInfo bottomLevelAccelerationStructureBuildFenceCreateInfo =
        {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = nullptr, .flags = 0};

    VkFence bottomLevelAccelerationStructureBuildFenceHandle = VK_NULL_HANDLE;
    result                                                   = vulkan_context.deviceFunctions(device)->vkCreateFence(
        device,
        &bottomLevelAccelerationStructureBuildFenceCreateInfo,
        nullptr,
        &bottomLevelAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateFence";
    }

    result = vulkan_context.deviceFunctions(device)->vkQueueSubmit(
        queueHandle,
        1,
        &bottomLevelAccelerationStructureBuildSubmitInfo,
        bottomLevelAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkQueueSubmit";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkWaitForFences(device, 1, &bottomLevelAccelerationStructureBuildFenceHandle, true, UINT32_MAX);

    if(result != VK_SUCCESS && result != VK_TIMEOUT)
    {
        qCritical() << result << "vkWaitForFences";
    }

    // =========================================================================
    // Top Level Acceleration Structure

    VkAccelerationStructureInstanceKHR bottomLevelAccelerationStructureInstance = {
        .transform                              = data->model->getVulkanTransform(),
        .instanceCustomIndex                    = 0,
        .mask                                   = 0xFF,
        .instanceShaderBindingTableRecordOffset = 0,
        .flags                                  = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR,
        .accelerationStructureReference         = bottomLevelAccelerationStructureDeviceAddress
    };

    VkBufferCreateInfo bottomLevelGeometryInstanceBufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size  = sizeof(VkAccelerationStructureInstanceKHR),
        .usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                 VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer bottomLevelGeometryInstanceBufferHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkCreateBuffer(
        device,
        &bottomLevelGeometryInstanceBufferCreateInfo,
        nullptr,
        &bottomLevelGeometryInstanceBufferHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements bottomLevelGeometryInstanceMemoryRequirements;
    vulkan_context.deviceFunctions(device)->vkGetBufferMemoryRequirements(
        device,
        bottomLevelGeometryInstanceBufferHandle,
        &bottomLevelGeometryInstanceMemoryRequirements
    );

    uint32_t bottomLevelGeometryInstanceMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((bottomLevelGeometryInstanceMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            bottomLevelGeometryInstanceMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo bottomLevelGeometryInstanceMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = bottomLevelGeometryInstanceMemoryRequirements.size,
        .memoryTypeIndex = bottomLevelGeometryInstanceMemoryTypeIndex
    };

    VkDeviceMemory bottomLevelGeometryInstanceDeviceMemoryHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &bottomLevelGeometryInstanceMemoryAllocateInfo,
        nullptr,
        &bottomLevelGeometryInstanceDeviceMemoryHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)->vkBindBufferMemory(
        device,
        bottomLevelGeometryInstanceBufferHandle,
        bottomLevelGeometryInstanceDeviceMemoryHandle,
        0
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostbottomLevelGeometryInstanceMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        bottomLevelGeometryInstanceDeviceMemoryHandle,
        0,
        sizeof(VkAccelerationStructureInstanceKHR),
        0,
        &hostbottomLevelGeometryInstanceMemoryBuffer
    );

    memcpy(
        hostbottomLevelGeometryInstanceMemoryBuffer,
        &bottomLevelAccelerationStructureInstance,
        sizeof(VkAccelerationStructureInstanceKHR)
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, bottomLevelGeometryInstanceDeviceMemoryHandle);

    VkBufferDeviceAddressInfo bottomLevelGeometryInstanceDeviceAddressInfo = {
        .sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext  = nullptr,
        .buffer = bottomLevelGeometryInstanceBufferHandle
    };

    VkDeviceAddress bottomLevelGeometryInstanceDeviceAddress =
        pvkGetBufferDeviceAddressKHR(device, &bottomLevelGeometryInstanceDeviceAddressInfo);

    VkAccelerationStructureGeometryDataKHR topLevelAccelerationStructureGeometryData = {
        .instances =
            {.sType           = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR,
                        .pNext           = nullptr,
                        .arrayOfPointers = VK_FALSE,
                        .data            = {.deviceAddress = bottomLevelGeometryInstanceDeviceAddress}}
    };

    VkAccelerationStructureGeometryKHR topLevelAccelerationStructureGeometry = {
        .sType        = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
        .pNext        = nullptr,
        .geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR,
        .geometry     = topLevelAccelerationStructureGeometryData,
        .flags        = VK_GEOMETRY_OPAQUE_BIT_KHR
    };

    VkAccelerationStructureBuildGeometryInfoKHR topLevelAccelerationStructureBuildGeometryInfo = {
        .sType                    = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
        .pNext                    = nullptr,
        .type                     = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
        .flags                    = 0,
        .mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
        .srcAccelerationStructure = VK_NULL_HANDLE,
        .dstAccelerationStructure = VK_NULL_HANDLE,
        .geometryCount            = 1,
        .pGeometries              = &topLevelAccelerationStructureGeometry,
        .ppGeometries             = nullptr,
        .scratchData              = {.deviceAddress = 0}
    };

    VkAccelerationStructureBuildSizesInfoKHR topLevelAccelerationStructureBuildSizesInfo = {
        .sType                     = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
        .pNext                     = nullptr,
        .accelerationStructureSize = 0,
        .updateScratchSize         = 0,
        .buildScratchSize          = 0
    };

    std::vector<uint32_t> topLevelMaxPrimitiveCountList = {1};

    pvkGetAccelerationStructureBuildSizesKHR(
        device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &topLevelAccelerationStructureBuildGeometryInfo,
        topLevelMaxPrimitiveCountList.data(),
        &topLevelAccelerationStructureBuildSizesInfo
    );

    VkBufferCreateInfo topLevelAccelerationStructureBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = topLevelAccelerationStructureBuildSizesInfo.accelerationStructureSize,
        .usage                 = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer topLevelAccelerationStructureBufferHandle = VK_NULL_HANDLE;
    result                                             = vulkan_context.deviceFunctions(device)->vkCreateBuffer(
        device,
        &topLevelAccelerationStructureBufferCreateInfo,
        nullptr,
        &topLevelAccelerationStructureBufferHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements topLevelAccelerationStructureMemoryRequirements;
    vulkan_context.deviceFunctions(device)->vkGetBufferMemoryRequirements(
        device,
        topLevelAccelerationStructureBufferHandle,
        &topLevelAccelerationStructureMemoryRequirements
    );

    uint32_t topLevelAccelerationStructureMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((topLevelAccelerationStructureMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ==
               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            topLevelAccelerationStructureMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo topLevelAccelerationStructureMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = nullptr,
        .allocationSize  = topLevelAccelerationStructureMemoryRequirements.size,
        .memoryTypeIndex = topLevelAccelerationStructureMemoryTypeIndex
    };

    VkDeviceMemory topLevelAccelerationStructureDeviceMemoryHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &topLevelAccelerationStructureMemoryAllocateInfo,
        nullptr,
        &topLevelAccelerationStructureDeviceMemoryHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)->vkBindBufferMemory(
        device,
        topLevelAccelerationStructureBufferHandle,
        topLevelAccelerationStructureDeviceMemoryHandle,
        0
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    VkAccelerationStructureCreateInfoKHR topLevelAccelerationStructureCreateInfo = {
        .sType         = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR,
        .pNext         = nullptr,
        .createFlags   = 0,
        .buffer        = topLevelAccelerationStructureBufferHandle,
        .offset        = 0,
        .size          = topLevelAccelerationStructureBuildSizesInfo.accelerationStructureSize,
        .type          = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
        .deviceAddress = 0
    };

    VkAccelerationStructureKHR topLevelAccelerationStructureHandle = VK_NULL_HANDLE;

    result = pvkCreateAccelerationStructureKHR(
        device,
        &topLevelAccelerationStructureCreateInfo,
        nullptr,
        &topLevelAccelerationStructureHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateAccelerationStructureKHR";
    }

    // =========================================================================
    // Build Top Level Acceleration Structure

    VkAccelerationStructureDeviceAddressInfoKHR topLevelAccelerationStructureDeviceAddressInfo = {
        .sType                 = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR,
        .pNext                 = nullptr,
        .accelerationStructure = topLevelAccelerationStructureHandle
    };

    VkDeviceAddress topLevelAccelerationStructureDeviceAddress =
        pvkGetAccelerationStructureDeviceAddressKHR(device, &topLevelAccelerationStructureDeviceAddressInfo);

    VkBufferCreateInfo topLevelAccelerationStructureScratchBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = topLevelAccelerationStructureBuildSizesInfo.buildScratchSize,
        .usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer topLevelAccelerationStructureScratchBufferHandle = VK_NULL_HANDLE;
    result                                                    = vulkan_context.deviceFunctions(device)->vkCreateBuffer(
        device,
        &topLevelAccelerationStructureScratchBufferCreateInfo,
        nullptr,
        &topLevelAccelerationStructureScratchBufferHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements topLevelAccelerationStructureScratchMemoryRequirements;
    vulkan_context.deviceFunctions(device)->vkGetBufferMemoryRequirements(
        device,
        topLevelAccelerationStructureScratchBufferHandle,
        &topLevelAccelerationStructureScratchMemoryRequirements
    );

    uint32_t topLevelAccelerationStructureScratchMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((topLevelAccelerationStructureScratchMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ==
               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            topLevelAccelerationStructureScratchMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo topLevelAccelerationStructureScratchMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = topLevelAccelerationStructureScratchMemoryRequirements.size,
        .memoryTypeIndex = topLevelAccelerationStructureScratchMemoryTypeIndex
    };

    VkDeviceMemory topLevelAccelerationStructureDeviceScratchMemoryHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &topLevelAccelerationStructureScratchMemoryAllocateInfo,
        nullptr,
        &topLevelAccelerationStructureDeviceScratchMemoryHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)->vkBindBufferMemory(
        device,
        topLevelAccelerationStructureScratchBufferHandle,
        topLevelAccelerationStructureDeviceScratchMemoryHandle,
        0
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    VkBufferDeviceAddressInfo topLevelAccelerationStructureScratchBufferDeviceAddressInfo = {
        .sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext  = nullptr,
        .buffer = topLevelAccelerationStructureScratchBufferHandle
    };

    VkDeviceAddress topLevelAccelerationStructureScratchBufferDeviceAddress =
        pvkGetBufferDeviceAddressKHR(device, &topLevelAccelerationStructureScratchBufferDeviceAddressInfo);

    topLevelAccelerationStructureBuildGeometryInfo.dstAccelerationStructure = topLevelAccelerationStructureHandle;

    topLevelAccelerationStructureBuildGeometryInfo.scratchData = {
        .deviceAddress = topLevelAccelerationStructureScratchBufferDeviceAddress
    };

    VkAccelerationStructureBuildRangeInfoKHR topLevelAccelerationStructureBuildRangeInfo =
        {.primitiveCount = 1, .primitiveOffset = 0, .firstVertex = 0, .transformOffset = 0};

    const VkAccelerationStructureBuildRangeInfoKHR* topLevelAccelerationStructureBuildRangeInfos =
        &topLevelAccelerationStructureBuildRangeInfo;

    VkCommandBufferBeginInfo topLevelCommandBufferBeginInfo = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext            = nullptr,
        .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr
    };

    result = vulkan_context.deviceFunctions(device)->vkBeginCommandBuffer(
        commandBufferHandleList.back(),
        &topLevelCommandBufferBeginInfo
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBeginCommandBuffer";
    }

    pvkCmdBuildAccelerationStructuresKHR(
        commandBufferHandleList.back(),
        1,
        &topLevelAccelerationStructureBuildGeometryInfo,
        &topLevelAccelerationStructureBuildRangeInfos
    );

    result = vulkan_context.deviceFunctions(device)->vkEndCommandBuffer(commandBufferHandleList.back());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkEndCommandBuffer";
    }

    VkSubmitInfo topLevelAccelerationStructureBuildSubmitInfo = {
        .sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext                = nullptr,
        .waitSemaphoreCount   = 0,
        .pWaitSemaphores      = nullptr,
        .pWaitDstStageMask    = nullptr,
        .commandBufferCount   = 1,
        .pCommandBuffers      = &commandBufferHandleList.back(),
        .signalSemaphoreCount = 0,
        .pSignalSemaphores    = nullptr
    };

    VkFenceCreateInfo topLevelAccelerationStructureBuildFenceCreateInfo =
        {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = nullptr, .flags = 0};

    VkFence topLevelAccelerationStructureBuildFenceHandle = VK_NULL_HANDLE;
    result                                                = vulkan_context.deviceFunctions(device)->vkCreateFence(
        device,
        &topLevelAccelerationStructureBuildFenceCreateInfo,
        nullptr,
        &topLevelAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateFence";
    }

    result = vulkan_context.deviceFunctions(device)->vkQueueSubmit(
        queueHandle,
        1,
        &topLevelAccelerationStructureBuildSubmitInfo,
        topLevelAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkQueueSubmit";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkWaitForFences(device, 1, &topLevelAccelerationStructureBuildFenceHandle, true, UINT32_MAX);

    if(result != VK_SUCCESS && result != VK_TIMEOUT)
    {
        qCritical() << result << "vkWaitForFences";
    }

    // =========================================================================
    // Uniform Buffer

    struct UniformStructure
    {
        float cameraPosition[4] = {-1.433908, 3.579997, 5.812919, 1};
        float cameraRight[4]    = {0.928479, 0, 0.371385, 1};
        float cameraUp[4]       = {0, 1, 0, 1};
        float cameraForward[4]  = {0.371385, 0, -0.928479, 1};

        uint32_t frameCount = 0;
    } uniformStructure;

    uniformStructure.cameraPosition[0] = data->camera.getPosition().x();
    uniformStructure.cameraPosition[1] = data->camera.getPosition().y();
    uniformStructure.cameraPosition[2] = data->camera.getPosition().z();

    QVector3D forward, up, right;
    data->camera.getRotation().getAxes(&forward, &up, &right);

    uniformStructure.cameraRight[0] = right.x();
    uniformStructure.cameraRight[1] = right.y();
    uniformStructure.cameraRight[2] = right.z();

    uniformStructure.cameraUp[0] = up.x();
    uniformStructure.cameraUp[1] = up.y();
    uniformStructure.cameraUp[2] = up.z();

    uniformStructure.cameraForward[0] = forward.x();
    uniformStructure.cameraForward[1] = forward.y();
    uniformStructure.cameraForward[2] = forward.z();

    VkBufferCreateInfo uniformBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = sizeof(UniformStructure),
        .usage                 = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer uniformBufferHandle = VK_NULL_HANDLE;
    result                       = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &uniformBufferCreateInfo, nullptr, &uniformBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements uniformMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, uniformBufferHandle, &uniformMemoryRequirements);

    uint32_t uniformMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((uniformMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            uniformMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo uniformMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = uniformMemoryRequirements.size,
        .memoryTypeIndex = uniformMemoryTypeIndex
    };

    VkDeviceMemory uniformDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                   = vulkan_context.deviceFunctions(device)
                 ->vkAllocateMemory(device, &uniformMemoryAllocateInfo, nullptr, &uniformDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, uniformBufferHandle, uniformDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostUniformMemoryBuffer;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkMapMemory(device, uniformDeviceMemoryHandle, 0, sizeof(UniformStructure), 0, &hostUniformMemoryBuffer);

    memcpy(hostUniformMemoryBuffer, &uniformStructure, sizeof(UniformStructure));

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, uniformDeviceMemoryHandle);

    // =========================================================================
    // Ray Trace Image

    VkImageCreateInfo rayTraceImageCreateInfo = {
        .sType     = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext     = nullptr,
        .flags     = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format    = VK_FORMAT_R8G8B8A8_UNORM,
        .extent =
            {.width  = static_cast<uint32_t>(data->rendering_size.width()),
                     .height = static_cast<uint32_t>(data->rendering_size.height()),
                     .depth  = 1},
        .mipLevels             = 1,
        .arrayLayers           = 1,
        .samples               = VK_SAMPLE_COUNT_1_BIT,
        .tiling                = VK_IMAGE_TILING_OPTIMAL,
        .usage                 = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index,
        .initialLayout         = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkImage rayTraceImageHandle = VK_NULL_HANDLE;

    result = vulkan_context.deviceFunctions(device)
                 ->vkCreateImage(device, &rayTraceImageCreateInfo, nullptr, &rayTraceImageHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateImage";
    }

    VkMemoryRequirements rayTraceImageMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetImageMemoryRequirements(device, rayTraceImageHandle, &rayTraceImageMemoryRequirements);

    uint32_t rayTraceImageMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((rayTraceImageMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ==
               VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            rayTraceImageMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo rayTraceImageMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = nullptr,
        .allocationSize  = rayTraceImageMemoryRequirements.size,
        .memoryTypeIndex = rayTraceImageMemoryTypeIndex
    };

    VkDeviceMemory rayTraceImageDeviceMemoryHandle = VK_NULL_HANDLE;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkAllocateMemory(device, &rayTraceImageMemoryAllocateInfo, nullptr, &rayTraceImageDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindImageMemory(device, rayTraceImageHandle, rayTraceImageDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindImageMemory";
    }

    VkImageViewCreateInfo rayTraceImageViewCreateInfo = {
        .sType    = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext    = nullptr,
        .flags    = 0,
        .image    = rayTraceImageHandle,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format   = VK_FORMAT_R8G8B8A8_UNORM,
        .components =
            {.r = VK_COMPONENT_SWIZZLE_IDENTITY,
                         .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                         .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                         .a = VK_COMPONENT_SWIZZLE_IDENTITY},
        .subresourceRange =
            {.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
                         .baseMipLevel   = 0,
                         .levelCount     = 1,
                         .baseArrayLayer = 0,
                         .layerCount     = 1}
    };

    VkImageView rayTraceImageViewHandle = VK_NULL_HANDLE;
    result                              = vulkan_context.deviceFunctions(device)
                 ->vkCreateImageView(device, &rayTraceImageViewCreateInfo, nullptr, &rayTraceImageViewHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateImageView";
    }

    // =========================================================================
    // Ray Trace Image Barrier
    // (VK_IMAGE_LAYOUT_UNDEFINED -> VK_IMAGE_LAYOUT_GENERAL)

    VkCommandBufferBeginInfo rayTraceImageBarrierCommandBufferBeginInfo = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext            = nullptr,
        .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr
    };

    result = vulkan_context.deviceFunctions(device)->vkBeginCommandBuffer(
        commandBufferHandleList.back(),
        &rayTraceImageBarrierCommandBufferBeginInfo
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBeginCommandBuffer";
    }

    VkImageMemoryBarrier rayTraceGeneralMemoryBarrier = {
        .sType               = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext               = nullptr,
        .srcAccessMask       = 0,
        .dstAccessMask       = 0,
        .oldLayout           = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout           = VK_IMAGE_LAYOUT_GENERAL,
        .srcQueueFamilyIndex = queue_family_index,
        .dstQueueFamilyIndex = queue_family_index,
        .image               = rayTraceImageHandle,
        .subresourceRange =
            {.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
                               .baseMipLevel   = 0,
                               .levelCount     = 1,
                               .baseArrayLayer = 0,
                               .layerCount     = 1}
    };

    vulkan_context.deviceFunctions(device)->vkCmdPipelineBarrier(
        commandBufferHandleList.back(),
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &rayTraceGeneralMemoryBarrier
    );

    result = vulkan_context.deviceFunctions(device)->vkEndCommandBuffer(commandBufferHandleList.back());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkEndCommandBuffer";
    }

    VkSubmitInfo rayTraceImageBarrierAccelerationStructureBuildSubmitInfo = {
        .sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext                = nullptr,
        .waitSemaphoreCount   = 0,
        .pWaitSemaphores      = nullptr,
        .pWaitDstStageMask    = nullptr,
        .commandBufferCount   = 1,
        .pCommandBuffers      = &commandBufferHandleList.back(),
        .signalSemaphoreCount = 0,
        .pSignalSemaphores    = nullptr
    };

    VkFenceCreateInfo rayTraceImageBarrierAccelerationStructureBuildFenceCreateInfo =
        {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = nullptr, .flags = 0};

    VkFence rayTraceImageBarrierAccelerationStructureBuildFenceHandle = VK_NULL_HANDLE;
    result = vulkan_context.deviceFunctions(device)->vkCreateFence(
        device,
        &rayTraceImageBarrierAccelerationStructureBuildFenceCreateInfo,
        nullptr,
        &rayTraceImageBarrierAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateFence";
    }

    result = vulkan_context.deviceFunctions(device)->vkQueueSubmit(
        queueHandle,
        1,
        &rayTraceImageBarrierAccelerationStructureBuildSubmitInfo,
        rayTraceImageBarrierAccelerationStructureBuildFenceHandle
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkQueueSubmit";
    }

    result =
        vulkan_context.deviceFunctions(device)
            ->vkWaitForFences(device, 1, &rayTraceImageBarrierAccelerationStructureBuildFenceHandle, true, UINT32_MAX);

    if(result != VK_SUCCESS && result != VK_TIMEOUT)
    {
        qCritical() << result << "vkWaitForFences";
    }

    // =========================================================================
    // Result Buffer

    VkBufferCreateInfo resultBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = rayTraceImageMemoryRequirements.size,
        .usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer resultBufferHandle = VK_NULL_HANDLE;
    result                      = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &resultBufferCreateInfo, nullptr, &resultBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements resultMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, resultBufferHandle, &resultMemoryRequirements);

    uint32_t resultMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((resultMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            resultMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo resultMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = resultMemoryRequirements.size,
        .memoryTypeIndex = resultMemoryTypeIndex
    };

    VkDeviceMemory resultDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                  = vulkan_context.deviceFunctions(device)
                 ->vkAllocateMemory(device, &resultMemoryAllocateInfo, nullptr, &resultDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, resultBufferHandle, resultDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    // =========================================================================
    // Update Descriptor Set

    VkWriteDescriptorSetAccelerationStructureKHR accelerationStructureDescriptorInfo = {
        .sType                      = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR,
        .pNext                      = nullptr,
        .accelerationStructureCount = 1,
        .pAccelerationStructures    = &topLevelAccelerationStructureHandle
    };

    VkDescriptorBufferInfo uniformDescriptorInfo = {.buffer = uniformBufferHandle, .offset = 0, .range = VK_WHOLE_SIZE};

    VkDescriptorBufferInfo indexDescriptorInfo = {.buffer = indexBufferHandle, .offset = 0, .range = VK_WHOLE_SIZE};

    VkDescriptorBufferInfo vertexDescriptorInfo = {.buffer = vertexBufferHandle, .offset = 0, .range = VK_WHOLE_SIZE};

    VkDescriptorImageInfo rayTraceImageDescriptorInfo =
        {.sampler = VK_NULL_HANDLE, .imageView = rayTraceImageViewHandle, .imageLayout = VK_IMAGE_LAYOUT_GENERAL};

    std::vector<VkWriteDescriptorSet> writeDescriptorSetList = {

        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = &accelerationStructureDescriptorInfo,
         .dstSet           = descriptorSetHandleList[0],
         .dstBinding       = 0,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR,
         .pImageInfo       = nullptr,
         .pBufferInfo      = nullptr,
         .pTexelBufferView = nullptr},
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[0],
         .dstBinding       = 1,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
         .pImageInfo       = nullptr,
         .pBufferInfo      = &uniformDescriptorInfo,
         .pTexelBufferView = nullptr},
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[0],
         .dstBinding       = 2,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .pImageInfo       = nullptr,
         .pBufferInfo      = &indexDescriptorInfo,
         .pTexelBufferView = nullptr},
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[0],
         .dstBinding       = 3,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .pImageInfo       = nullptr,
         .pBufferInfo      = &vertexDescriptorInfo,
         .pTexelBufferView = nullptr},
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[0],
         .dstBinding       = 4,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
         .pImageInfo       = &rayTraceImageDescriptorInfo,
         .pBufferInfo      = nullptr,
         .pTexelBufferView = nullptr}
    };

    vulkan_context.deviceFunctions(device)
        ->vkUpdateDescriptorSets(device, writeDescriptorSetList.size(), writeDescriptorSetList.data(), 0, nullptr);

    // =========================================================================
    // Material Index Buffer

    std::vector<uint32_t> materialIndexList;
    materialIndexList.push_back(0);

    VkBufferCreateInfo materialIndexBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = sizeof(uint32_t) * materialIndexList.size(),
        .usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer materialIndexBufferHandle = VK_NULL_HANDLE;
    result                             = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &materialIndexBufferCreateInfo, nullptr, &materialIndexBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements materialIndexMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, materialIndexBufferHandle, &materialIndexMemoryRequirements);

    uint32_t materialIndexMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((materialIndexMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            materialIndexMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo materialIndexMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = materialIndexMemoryRequirements.size,
        .memoryTypeIndex = materialIndexMemoryTypeIndex
    };

    VkDeviceMemory materialIndexDeviceMemoryHandle = VK_NULL_HANDLE;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkAllocateMemory(device, &materialIndexMemoryAllocateInfo, nullptr, &materialIndexDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, materialIndexBufferHandle, materialIndexDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostMaterialIndexMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        materialIndexDeviceMemoryHandle,
        0,
        sizeof(uint32_t) * materialIndexList.size(),
        0,
        &hostMaterialIndexMemoryBuffer
    );

    memcpy(hostMaterialIndexMemoryBuffer, materialIndexList.data(), sizeof(uint32_t) * materialIndexList.size());

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, materialIndexDeviceMemoryHandle);

    // =========================================================================
    // Material Buffer

    struct Material
    {
        float ambient[4]  = {0, 1, 0, 0};
        float diffuse[4]  = {0, 1, 0, 0};
        float specular[4] = {0, 1, 0, 0};
        float emission[4] = {0, 1, 0, 0};
    } material;

    VkBufferCreateInfo materialBufferCreateInfo = {
        .sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext                 = nullptr,
        .flags                 = 0,
        .size                  = sizeof(Material),
        .usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        .sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer materialBufferHandle = VK_NULL_HANDLE;
    result                        = vulkan_context.deviceFunctions(device)
                 ->vkCreateBuffer(device, &materialBufferCreateInfo, nullptr, &materialBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements materialMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, materialBufferHandle, &materialMemoryRequirements);

    uint32_t materialMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((materialMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            materialMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo materialMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = materialMemoryRequirements.size,
        .memoryTypeIndex = materialMemoryTypeIndex
    };

    VkDeviceMemory materialDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                    = vulkan_context.deviceFunctions(device)
                 ->vkAllocateMemory(device, &materialMemoryAllocateInfo, nullptr, &materialDeviceMemoryHandle);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, materialBufferHandle, materialDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    void* hostMaterialMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)
                 ->vkMapMemory(device, materialDeviceMemoryHandle, 0, sizeof(Material), 0, &hostMaterialMemoryBuffer);

    memcpy(hostMaterialMemoryBuffer, &material, sizeof(Material));

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, materialDeviceMemoryHandle);

    // =========================================================================
    // Update Material Descriptor Set

    VkDescriptorBufferInfo materialIndexDescriptorInfo =
        {.buffer = materialIndexBufferHandle, .offset = 0, .range = VK_WHOLE_SIZE};

    VkDescriptorBufferInfo materialDescriptorInfo =
        {.buffer = materialBufferHandle, .offset = 0, .range = VK_WHOLE_SIZE};

    std::vector<VkWriteDescriptorSet> materialWriteDescriptorSetList = {
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[1],
         .dstBinding       = 0,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .pImageInfo       = nullptr,
         .pBufferInfo      = &materialIndexDescriptorInfo,
         .pTexelBufferView = nullptr},
        {.sType            = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
         .pNext            = nullptr,
         .dstSet           = descriptorSetHandleList[1],
         .dstBinding       = 1,
         .dstArrayElement  = 0,
         .descriptorCount  = 1,
         .descriptorType   = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
         .pImageInfo       = nullptr,
         .pBufferInfo      = &materialDescriptorInfo,
         .pTexelBufferView = nullptr}
    };

    vulkan_context.deviceFunctions(device)->vkUpdateDescriptorSets(
        device,
        materialWriteDescriptorSetList.size(),
        materialWriteDescriptorSetList.data(),
        0,
        nullptr
    );

    // =========================================================================
    // Shader Binding Table

    VkPhysicalDeviceRayTracingPipelinePropertiesKHR physicalDeviceRayTracingPipelineProperties = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR,
        .pNext = nullptr
    };

    VkPhysicalDeviceProperties physicalDeviceProperties;
    vulkan_context.functions()->vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);

    VkPhysicalDeviceProperties2 physicalDeviceProperties2 = {
        .sType      = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
        .pNext      = &physicalDeviceRayTracingPipelineProperties,
        .properties = physicalDeviceProperties
    };

    vulkan_context.functions()->vkGetPhysicalDeviceProperties2(device, &physicalDeviceProperties2);

    VkDeviceSize progSize = physicalDeviceRayTracingPipelineProperties.shaderGroupBaseAlignment;

    VkDeviceSize shaderBindingTableSize = progSize * 4;

    VkBufferCreateInfo shaderBindingTableBufferCreateInfo = {
        .sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext       = nullptr,
        .flags       = 0,
        .size        = shaderBindingTableSize,
        .usage       = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1,
        .pQueueFamilyIndices   = &queue_family_index
    };

    VkBuffer shaderBindingTableBufferHandle = VK_NULL_HANDLE;
    result =
        vulkan_context.deviceFunctions(device)
            ->vkCreateBuffer(device, &shaderBindingTableBufferCreateInfo, nullptr, &shaderBindingTableBufferHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateBuffer";
    }

    VkMemoryRequirements shaderBindingTableMemoryRequirements;
    vulkan_context.deviceFunctions(device)
        ->vkGetBufferMemoryRequirements(device, shaderBindingTableBufferHandle, &shaderBindingTableMemoryRequirements);

    uint32_t shaderBindingTableMemoryTypeIndex = -1;
    for(uint32_t x = 0; x < physicalDeviceMemoryProperties.memoryTypeCount; x++)
    {
        if((shaderBindingTableMemoryRequirements.memoryTypeBits & (1 << x)) &&
           (physicalDeviceMemoryProperties.memoryTypes[x].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ==
               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            shaderBindingTableMemoryTypeIndex = x;
            break;
        }
    }

    VkMemoryAllocateInfo shaderBindingTableMemoryAllocateInfo = {
        .sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext           = &memoryAllocateFlagsInfo,
        .allocationSize  = shaderBindingTableMemoryRequirements.size,
        .memoryTypeIndex = shaderBindingTableMemoryTypeIndex
    };

    VkDeviceMemory shaderBindingTableDeviceMemoryHandle = VK_NULL_HANDLE;
    result                                              = vulkan_context.deviceFunctions(device)->vkAllocateMemory(
        device,
        &shaderBindingTableMemoryAllocateInfo,
        nullptr,
        &shaderBindingTableDeviceMemoryHandle
    );
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkAllocateMemory";
    }

    result = vulkan_context.deviceFunctions(device)
                 ->vkBindBufferMemory(device, shaderBindingTableBufferHandle, shaderBindingTableDeviceMemoryHandle, 0);
    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBindBufferMemory";
    }

    char* shaderHandleBuffer = new char[shaderBindingTableSize];
    result                   = pvkGetRayTracingShaderGroupHandlesKHR(
        device,
        rayTracingPipelineHandle,
        0,
        4,
        shaderBindingTableSize,
        shaderHandleBuffer
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkGetRayTracingShaderGroupHandlesKHR";
    }

    void* hostShaderBindingTableMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        shaderBindingTableDeviceMemoryHandle,
        0,
        shaderBindingTableSize,
        0,
        &hostShaderBindingTableMemoryBuffer
    );

    for(uint32_t x = 0; x < 4; x++)
    {
        memcpy(
            hostShaderBindingTableMemoryBuffer,
            shaderHandleBuffer + x * physicalDeviceRayTracingPipelineProperties.shaderGroupHandleSize,
            physicalDeviceRayTracingPipelineProperties.shaderGroupHandleSize
        );

        hostShaderBindingTableMemoryBuffer = reinterpret_cast<char*>(hostShaderBindingTableMemoryBuffer) +
                                             physicalDeviceRayTracingPipelineProperties.shaderGroupBaseAlignment;
    }

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, shaderBindingTableDeviceMemoryHandle);

    VkBufferDeviceAddressInfo shaderBindingTableBufferDeviceAddressInfo = {
        .sType  = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext  = nullptr,
        .buffer = shaderBindingTableBufferHandle
    };

    VkDeviceAddress shaderBindingTableBufferDeviceAddress =
        pvkGetBufferDeviceAddressKHR(device, &shaderBindingTableBufferDeviceAddressInfo);

    VkDeviceSize hitGroupOffset = 0u * progSize;
    VkDeviceSize rayGenOffset   = 1u * progSize;
    VkDeviceSize missOffset     = 2u * progSize;

    const VkStridedDeviceAddressRegionKHR rchitShaderBindingTable =
        {.deviceAddress = shaderBindingTableBufferDeviceAddress + hitGroupOffset, .stride = progSize, .size = progSize};

    const VkStridedDeviceAddressRegionKHR rgenShaderBindingTable =
        {.deviceAddress = shaderBindingTableBufferDeviceAddress + rayGenOffset, .stride = progSize, .size = progSize};

    const VkStridedDeviceAddressRegionKHR rmissShaderBindingTable =
        {.deviceAddress = shaderBindingTableBufferDeviceAddress + missOffset, .stride = progSize, .size = progSize * 2};

    const VkStridedDeviceAddressRegionKHR callableShaderBindingTable = {};

    // =========================================================================
    // Record Render Pass Command Buffer

    VkCommandBufferBeginInfo renderCommandBufferBeginInfo = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext            = nullptr,
        .flags            = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
        .pInheritanceInfo = nullptr
    };

    result = vulkan_context.deviceFunctions(device)->vkBeginCommandBuffer(
        commandBufferHandleList[0],
        &renderCommandBufferBeginInfo
    );

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkBeginCommandBuffer";
    }

    vulkan_context.deviceFunctions(device)->vkCmdBindPipeline(
        commandBufferHandleList[0],
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
        rayTracingPipelineHandle
    );

    vulkan_context.deviceFunctions(device)->vkCmdBindDescriptorSets(
        commandBufferHandleList[0],
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
        pipelineLayoutHandle,
        0,
        (uint32_t)descriptorSetHandleList.size(),
        descriptorSetHandleList.data(),
        0,
        nullptr
    );

    pvkCmdTraceRaysKHR(
        commandBufferHandleList[0],
        &rgenShaderBindingTable,
        &rmissShaderBindingTable,
        &rchitShaderBindingTable,
        &callableShaderBindingTable,
        static_cast<uint32_t>(data->rendering_size.width()),
        static_cast<uint32_t>(data->rendering_size.height()),
        1
    );

    VkImageMemoryBarrier rayTraceCopyMemoryBarrier = {
        .sType               = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext               = nullptr,
        .srcAccessMask       = 0,
        .dstAccessMask       = VK_ACCESS_TRANSFER_READ_BIT,
        .oldLayout           = VK_IMAGE_LAYOUT_GENERAL,
        .newLayout           = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        .srcQueueFamilyIndex = queue_family_index,
        .dstQueueFamilyIndex = queue_family_index,
        .image               = rayTraceImageHandle,
        .subresourceRange =
            {.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
                               .baseMipLevel   = 0,
                               .levelCount     = 1,
                               .baseArrayLayer = 0,
                               .layerCount     = 1}
    };

    vulkan_context.deviceFunctions(device)->vkCmdPipelineBarrier(
        commandBufferHandleList[0],
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &rayTraceCopyMemoryBarrier
    );

    VkBufferImageCopy imageCopy = {
        .bufferOffset      = 0,
        .bufferRowLength   = 0,
        .bufferImageHeight = 0,
        .imageSubresource =
            {.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .mipLevel = 0, .baseArrayLayer = 0, .layerCount = 1},
        .imageOffset = 0,
        .imageExtent =
            {.width  = static_cast<uint32_t>(data->rendering_size.width()),
                               .height = static_cast<uint32_t>(data->rendering_size.height()),
                               .depth  = 1}
    };

    vulkan_context.deviceFunctions(device)->vkCmdCopyImageToBuffer(
        commandBufferHandleList[0],
        rayTraceImageHandle,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        resultBufferHandle,
        1,
        &imageCopy
    );

    result = vulkan_context.deviceFunctions(device)->vkEndCommandBuffer(commandBufferHandleList[0]);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkEndCommandBuffer";
    }

    // =========================================================================
    // Fence

    VkFence imageAvailableFenceHandle;

    VkFenceCreateInfo imageAvailableFenceCreateInfo =
        {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = nullptr, .flags = 0};

    result = vulkan_context.deviceFunctions(device)
                 ->vkCreateFence(device, &imageAvailableFenceCreateInfo, nullptr, &imageAvailableFenceHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkCreateFence";
    }

    // =========================================================================
    // Submit Command Buffer

    VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {
        .sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext                = nullptr,
        .waitSemaphoreCount   = 0,
        .pWaitSemaphores      = nullptr,
        .pWaitDstStageMask    = &pipelineStageFlags,
        .commandBufferCount   = 1,
        .pCommandBuffers      = &commandBufferHandleList[0],
        .signalSemaphoreCount = 0,
        .pSignalSemaphores    = nullptr
    };

    result =
        vulkan_context.deviceFunctions(device)->vkQueueSubmit(queueHandle, 1, &submitInfo, imageAvailableFenceHandle);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkQueueSubmit";
    }

    // =========================================================================
    // Read Image From Buffer

    vulkan_context.deviceFunctions(device)->vkWaitForFences(device, 1, &imageAvailableFenceHandle, VK_TRUE, 0);

    void* hostResultMemoryBuffer;
    result = vulkan_context.deviceFunctions(device)->vkMapMemory(
        device,
        resultDeviceMemoryHandle,
        0,
        rayTraceImageMemoryRequirements.size,
        0,
        &hostResultMemoryBuffer
    );

    QImage image(
        (uchar*)hostResultMemoryBuffer,
        data->rendering_size.width(),
        data->rendering_size.height(),
        QImage::Format_RGBA8888
    );

    image.save("enginee.png");
    data->last_render = image.copy();

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkMapMemory";
    }

    vulkan_context.deviceFunctions(device)->vkUnmapMemory(device, resultDeviceMemoryHandle);

    // =========================================================================
    // Cleanup

    result = vulkan_context.deviceFunctions(device)->vkDeviceWaitIdle(device);

    if(result != VK_SUCCESS)
    {
        qCritical() << result << "vkDeviceWaitIdle";
    }

    vulkan_context.deviceFunctions(device)->vkDestroyFence(device, imageAvailableFenceHandle, nullptr);

    delete[] shaderHandleBuffer;
    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, shaderBindingTableDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, shaderBindingTableBufferHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, materialDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, materialBufferHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, materialIndexDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, materialIndexBufferHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, resultDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, resultBufferHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkDestroyFence(device, rayTraceImageBarrierAccelerationStructureBuildFenceHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkDestroyImageView(device, rayTraceImageViewHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, rayTraceImageDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyImage(device, rayTraceImageHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, uniformDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, uniformBufferHandle, nullptr);
    vulkan_context.deviceFunctions(device)
        ->vkDestroyFence(device, topLevelAccelerationStructureBuildFenceHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkFreeMemory(device, topLevelAccelerationStructureDeviceScratchMemoryHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkDestroyBuffer(device, topLevelAccelerationStructureScratchBufferHandle, nullptr);

    pvkDestroyAccelerationStructureKHR(device, topLevelAccelerationStructureHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkFreeMemory(device, topLevelAccelerationStructureDeviceMemoryHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, topLevelAccelerationStructureBufferHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkFreeMemory(device, bottomLevelGeometryInstanceDeviceMemoryHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, bottomLevelGeometryInstanceBufferHandle, nullptr);
    vulkan_context.deviceFunctions(device)
        ->vkDestroyFence(device, bottomLevelAccelerationStructureBuildFenceHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkFreeMemory(device, bottomLevelAccelerationStructureDeviceScratchMemoryHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkDestroyBuffer(device, bottomLevelAccelerationStructureScratchBufferHandle, nullptr);

    pvkDestroyAccelerationStructureKHR(device, bottomLevelAccelerationStructureHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkFreeMemory(device, bottomLevelAccelerationStructureDeviceMemoryHandle, nullptr);

    vulkan_context.deviceFunctions(device)
        ->vkDestroyBuffer(device, bottomLevelAccelerationStructureBufferHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, indexDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, indexBufferHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkFreeMemory(device, vertexDeviceMemoryHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyBuffer(device, vertexBufferHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyPipeline(device, rayTracingPipelineHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyShaderModule(device, rayMissShadowShaderModuleHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyShaderModule(device, rayMissShaderModuleHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyShaderModule(device, rayGenerateShaderModuleHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyShaderModule(device, rayClosestHitShaderModuleHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyPipelineLayout(device, pipelineLayoutHandle, nullptr);
    vulkan_context.deviceFunctions(device)
        ->vkDestroyDescriptorSetLayout(device, materialDescriptorSetLayoutHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkDestroyDescriptorSetLayout(device, descriptorSetLayoutHandle, nullptr);
    vulkan_context.deviceFunctions(device)->vkDestroyDescriptorPool(device, descriptorPoolHandle, nullptr);

    vulkan_context.deviceFunctions(device)->vkDestroyCommandPool(device, commandPoolHandle, nullptr);

    qDebug() << frames++;
}

std::string VulkanRTRenderingStage::getName()
{
    return "VulkanRTRenderingStage";
}

VulkanRTRenderingStage::~VulkanRTRenderingStage()
{
    vulkan_context.deviceFunctions(device.logical_device)->vkDestroyDevice(device.logical_device, nullptr);
    vulkan_context.destroy();
}
