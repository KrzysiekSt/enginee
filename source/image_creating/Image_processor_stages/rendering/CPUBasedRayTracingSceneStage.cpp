#include "CPUBasedRayTracingSceneStage.h"

int CPUBasedRayTracingSceneStage::IntersectRaySphere(QVector3D p, QVector3D d, Sphere s, float& t, QVector3D& q)
{
    QVector3D m = p - s.pos;
    float     b = QVector3D::dotProduct(m, d);
    float     c = QVector3D::dotProduct(m, m) - s.radius * s.radius;

    // Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0)
    if(c > 0.0f && b > 0.0f) return 0;
    float discr = b * b - c;

    // A negative discriminant corresponds to ray missing sphere
    if(discr < 0.0f) return 0;

    // Ray now found to intersect sphere, compute smallest t value of intersection
    t = -b - sqrt(discr);

    // If t is negative, ray started inside sphere so clamp t to zero
    if(t < 0.0f) t = 0.0f;
    q = p + t * d;

    return 1;
}

CPUBasedRayTracingSceneStage::RayCastResult CPUBasedRayTracingSceneStage::rayVSSphere(Ray& ray, const Sphere& sphere)
{
    RayCastResult result;

    float     along = 0;
    QVector3D hitPoint;
    int       isHit = IntersectRaySphere(ray.origin, ray.direction.normalized(), sphere, along, hitPoint);

    result.hasHit = (isHit == 1);

    if(result.hasHit)
    {
        result.hitPoint  = hitPoint;
        result.hitNormal = (hitPoint - sphere.pos).normalized();
        ray.color        = ray.color - (QVector3D {1, 1, 1} - sphere.material.albedo);
    }

    return result;
}

CPUBasedRayTracingSceneStage::SceneCastResult CPUBasedRayTracingSceneStage::RayVsScene(const Scene& scene, Ray ray)
{
    SceneCastResult result;

    float closestHitDistance = 100000;
    for(const Sphere& sphere: scene.spheres)
    {
        RayCastResult res = rayVSSphere(ray, sphere);
        if(res.hasHit)
        {
            result.hasHit     = true;
            float hitDistance = scene.camera.pos.distanceToPoint(res.hitPoint);
            if(hitDistance < closestHitDistance)
            {
                closestHitDistance = hitDistance;

                result.material = sphere.material;
                result.point    = res.hitPoint;
                result.normal   = res.hitNormal;
            }
        }
    }

    return result;
}

QVector3D CPUBasedRayTracingSceneStage::randomVec()
{
    std::uniform_real_distribution<float> distribution(-1, 1);

    return {distribution(generator), distribution(generator), distribution(generator)};
}

QVector3D CPUBasedRayTracingSceneStage::diffuse(QVector3D normal)
{
    QVector3D randomVector = randomVec();
    if(QVector3D::dotProduct(randomVector, normal) < 0) randomVector *= -1.f;
    return randomVector.normalized();
}

QVector3D CPUBasedRayTracingSceneStage::Trace(const Scene& scene, const Ray ray)
{
    QVector3D incomingLight(0, 0, 0);
    QVector3D rayColor(1, 1, 1);
    Ray       newRay = ray;

    for(uint32_t i = 0; i < NumBounces; i++)
    {
        SceneCastResult result = RayVsScene(scene, newRay);
        if(result.hasHit)
        {
            newRay.origin    = result.point;
            QVector3D oldDir = newRay.direction;

            QVector3D                             diffuseDir  = diffuse(result.normal);
            QVector3D                             specularDir = reflect(oldDir, result.normal);
            std::uniform_real_distribution<float> distribution(0, 1);
            bool  isSpecularBounce = result.material.someProbability >= distribution(generator);
            float factor           = result.material.roughness * static_cast<float>(!isSpecularBounce);
            newRay.direction       = (1 - factor) * specularDir + factor * diffuseDir;

            QVector3D emittedLight  = result.material.emissive;
            float     lightStrength = qMax(QVector3D::dotProduct(result.normal, newRay.direction), 0.f);
            incomingLight += emittedLight * rayColor;

            factor         = static_cast<float>(isSpecularBounce);
            QVector3D lerp = (1 - factor) * (result.material.albedo * lightStrength) + factor * newRay.color;
            rayColor *= lerp;
        }
        else
        {
            break;
        }
    }

    return incomingLight;
}

void CPUBasedRayTracingSceneStage::render(const Scene& scene, Pixel* image)
{
    const float near   = -5;
    auto&       camera = scene.camera;

    const float planeY = near * std::tan(camera.fovy / 2.f);
    const float planeX = planeY * camera.aspect;

    std::uniform_real_distribution<float> offsetDistribution(-1, 1);

    const float TexelWidth  = planeX / static_cast<float>(ImageWidth);
    const float TexelHeight = planeY / static_cast<float>(ImageHeight);

    const float NumSamples = 1500.f;

    auto renderPart = [&](int start, int step)
    {
        for(int y = start; y < start + step; ++y)
        {
            printf("row %d/%d...\n", y + 1, ImageHeight);

            for(int x = 0; x < ImageWidth; ++x)
            {
                QVector3D color = {0, 0, 0};

                for(uint32_t i = 0; i < NumSamples; i++)
                {
                    QVector3D rayDirection {
                        static_cast<float>(x) / static_cast<float>(ImageWidth) * planeX - (planeX / 2),
                        static_cast<float>(y) / static_cast<float>(ImageHeight) * planeY - (planeY / 2),
                        near
                    };

                    float offsetX = offsetDistribution(generator);
                    float offsetY = offsetDistribution(generator);

                    rayDirection.setX(rayDirection.x() + offsetX * TexelWidth);
                    rayDirection.setY(rayDirection.y() + offsetY * TexelHeight);

                    rayDirection.normalize();

                    QMatrix4x4 inverseView = camera.view.inverted();
                    QVector4D  rayDirection4D(rayDirection, 0.0f);
                    QVector4D  result4D                 = inverseView * rayDirection4D;
                    QVector3D  rayDirectionInWorldSpace = QVector3D {result4D.x(), result4D.y(), result4D.z()};

                    Ray ray;
                    ray.origin    = camera.pos;
                    ray.direction = rayDirectionInWorldSpace;

                    color += Trace(scene, ray);
                }

                color /= NumSamples;

                color                       = color / (color + QVector3D(1, 1, 1));
                image[y * ImageWidth + x].r = static_cast<uint8_t>(color.x() * 255);
                image[y * ImageWidth + x].g = static_cast<uint8_t>(color.y() * 255);
                image[y * ImageWidth + x].b = static_cast<uint8_t>(color.z() * 255);
            }
        }
    };

    std::vector<std::thread> working_pool = {};
    for(int z = 0; z < 18; ++z)
    {
        int         ImageHeightPart = ImageHeight / 18;
        std::thread t(renderPart, ImageHeightPart * z, ImageHeightPart);
        working_pool.push_back(std::move(t));
    }
    for(auto& thread: working_pool)
    {
        thread.join();
    }
}

QVector3D CPUBasedRayTracingSceneStage::reflect(QVector3D dir, QVector3D normal)
{
    float dotProduct = QVector3D::dotProduct(dir, normal);
    return dir - 2 * dotProduct * normal;
}

CPUBasedRayTracingSceneStage::CPUBasedRayTracingSceneStage() {}
CPUBasedRayTracingSceneStage::~CPUBasedRayTracingSceneStage() {}
void CPUBasedRayTracingSceneStage::init(std::shared_ptr<ImageProcessorData> given_data)
{
    ImageProcessorStage::init(given_data);
    ImageWidth  = given_data->rendering_size.width();
    ImageHeight = given_data->rendering_size.height();

    //=================================

    const uint32_t NumPixels = ImageWidth * ImageHeight;
    Pixel*         pixels    = new Pixel[NumPixels];
    memset(pixels, 0, sizeof(Pixel) * NumPixels);

    Sphere sphere;
    sphere.pos             = QVector3D(0, 0, 0);
    sphere.radius          = 4.f;
    sphere.material.albedo = {1, 0, 0};

    Sphere sphere2;
    sphere2.pos                = QVector3D(0, -10, -15);
    sphere2.radius             = 10.f;
    sphere2.material.albedo    = {1, 1, 1};
    sphere2.material.roughness = 0.75;

    Sphere lightSphere;
    lightSphere.pos               = QVector3D(0, 0, -30);
    lightSphere.radius            = 3.f;
    lightSphere.material.emissive = QVector3D(500, 0, 0);

    Sphere lightSphere2;
    lightSphere2.pos               = QVector3D(10, 10, -40);
    lightSphere2.radius            = 5.f;
    lightSphere2.material.emissive = QVector3D(50, 50, 50);

    Camera    camera;
    QVector3D eyePos = QVector3D(30, 30, 30);
    QVector3D target = QVector3D(0, 0, 0);
    camera.setPosition(eyePos);
    camera.setDirection(target - eyePos);
    camera.fovy   = qDegreesToRadians(60.f);
    camera.aspect = (static_cast<float>(ImageWidth) / static_cast<float>(ImageHeight));

    Scene scene {
        camera,
        {sphere, sphere2, lightSphere, lightSphere2}
    };
    render(scene, pixels);

    output = QImage(ImageWidth, ImageHeight, QImage::Format_RGB32);

    for(int x = 0; x < output.width(); x++)
        for(int y = 0; y < output.height(); y++)
        {
            Pixel& pixel = pixels[y * output.width() + x];
            output.setPixelColor(x, y, QColor(pixel.r, pixel.g, pixel.b));
        }

    delete[] pixels;

    data->last_render = output.copy();
}

void CPUBasedRayTracingSceneStage::process()
{
    data->last_render = output.copy();
}

std::string CPUBasedRayTracingSceneStage::getName()
{
    return "CPUBasedRayTracingSceneStage";
}
