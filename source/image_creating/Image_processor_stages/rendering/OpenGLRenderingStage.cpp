#include "OpenGLRenderingStage.h"

OpenGLRenderingStage::OpenGLRenderingStage()
{
    setupContext();
    opengl_context.functions()->glClearColor(1.f, 1.f, 1.f, 1.0f);
    compileShaders();
    createBuffers();
}

void OpenGLRenderingStage::setupContext()
{
    opengl_context.create();
    surface_format.setVersion(4, 6);
    surface.setFormat(surface_format);
    surface.create();
    opengl_context.makeCurrent(&surface);
}

void OpenGLRenderingStage::createBuffers()
{
    vertex_buffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    vertex_buffer.create();
    vertex_buffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    vertex_buffer.bind();

    index_buffer = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    index_buffer.create();
    index_buffer.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    index_buffer.bind();
}

void OpenGLRenderingStage::compileShaders()
{
    // I want to keep shaders source in the exe, so I don't have to move shaders files around.
    // But I don't like Writing shaders in cpp file, so I just copy/paste it. Shaders headers are created every build -
    // look cmake
    QString vertex_shader =
#include "image_creating/shaders/headers/VertexShader.h"
        ;

    QString fragment_shader =
#include "image_creating/shaders/headers/FragmentShader.h"
        ;

    if(!shaders.addShaderFromSourceCode(QOpenGLShader::Vertex, vertex_shader))
        qDebug() << "Vertex shader errors:\n" << shaders.log();
    if(!shaders.addShaderFromSourceCode(QOpenGLShader::Fragment, fragment_shader))
        qDebug() << "Fragment shader errors:\n" << shaders.log();
    if(!shaders.link())
    {
        qDebug() << "Shader linker errors:\n" << shaders.log();
        shaders.removeAllShaders();
    }
}

void OpenGLRenderingStage::init(std::shared_ptr<ImageProcessorData> given_data)
{
    ImageProcessorStage::init(given_data);

    frame_buffer_object_format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    frame_buffer_object = std::make_unique<QOpenGLFramebufferObject>(
        data->rendering_size.width(),
        data->rendering_size.height(),
        frame_buffer_object_format
    );
    frame_buffer_object->setAttachment(QOpenGLFramebufferObject::Depth);
    frame_buffer_object->bind();
    if(!frame_buffer_object->isValid()) qDebug() << "Failed to create Framebuffer Object.";

    opengl_context.functions()->glViewport(0, 0, frame_buffer_object->width(), frame_buffer_object->height());

    auto positions = data->model->getVerticesPositions();
    vertex_buffer.allocate(positions.data(), positions.size() * sizeof(QVector3D));

    auto indexes = data->model->getFaces();
    index_buffer.allocate(indexes.data(), indexes.size() * sizeof(std::array<unsigned int, 3>));

    shaders.enableAttributeArray(vertex_attribute_index);
    shaders.setAttributeBuffer(
        vertex_attribute_index,
        GL_FLOAT,
        offset_to_attribute,
        length_of_position_vector,
        sizeof(QVector3D)
    );

    shaders.bind();
}

void OpenGLRenderingStage::process()
{
    qDebug() << frames++;
    if(data->model->isValid()) renderModel();
    else
    {
        opengl_context.functions()->glClear(GL_COLOR_BUFFER_BIT);
    }

    data->last_render = frame_buffer_object->toImage();
    // data->last_render.save("enginee.png");
}

void OpenGLRenderingStage::renderModel()
{
    auto faces         = data->model->getFaces();
    int  indexes_count = faces.size() * 3;

    auto view_projection_value = data->camera.getProjection() * data->camera.getView();
    shaders.setUniformValueArray(projection_view_uniform_name.c_str(), &view_projection_value, 1);

    auto model_value = data->model->getTransform();
    shaders.setUniformValueArray(model_uniform_name.c_str(), &model_value, 1);

    opengl_context.functions()->glClear(GL_COLOR_BUFFER_BIT);
    shaders.setUniformValueArray(model_uniform_color_name.c_str(), &model_color, 1);
    opengl_context.functions()->glDrawElements(GL_TRIANGLES, indexes_count, GL_UNSIGNED_INT, nullptr);
    shaders.setUniformValueArray(model_uniform_color_name.c_str(), &edge_color, 1);
    opengl_context.functions()->glDrawElements(GL_LINES, indexes_count, GL_UNSIGNED_INT, nullptr);
}

std::string OpenGLRenderingStage::getName()
{
    return "OpenGLRenderingStage";
}
OpenGLRenderingStage::~OpenGLRenderingStage()
{
    shaders.release();
    shaders.removeAllShaders();
}
