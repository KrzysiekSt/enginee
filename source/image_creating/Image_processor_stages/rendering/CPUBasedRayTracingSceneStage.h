#pragma once

#include "image_creating/ImageProcessorStage.h"

#include <QColor>
#include <QMatrix4x4>
#include <QRgb>
#include <QVector3D>
#include <deque>
#include <random>
#include <thread>
#include <utility>
#include <vector>

class CPUBasedRayTracingSceneStage : public ImageProcessorStage
{
    QImage output;
    struct Pixel
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };

    struct Material
    {
        QVector3D albedo          = {1, 1, 1};
        QVector3D emissive        = {0, 0, 0};
        float     someProbability = 0.7;
        float     roughness       = 1;
    };

    struct Sphere
    {
        QVector3D pos;
        float     radius;
        Material  material;
    };

    struct Camera
    {
        void setPosition(QVector3D in_pos)
        {
            pos = in_pos;
            view.lookAt(pos, pos + dir, {0, 1, 0});
        };
        void setDirection(QVector3D in_dir)
        {
            dir = in_dir.normalized();
            view.lookAt(pos, pos + dir, {0, 1, 0});
        };
        QVector3D  pos;
        QVector3D  dir;
        QMatrix4x4 view;
        float      fovy;
        float      aspect;
    };

    struct Ray
    {
        QVector3D origin;
        QVector3D direction;
        QVector3D color = {1, 1, 1};
    };

    struct RayCastResult
    {
        bool      hasHit = false;
        QVector3D hitPoint;
        QVector3D hitNormal;
    };

    struct Scene
    {
        Camera              camera;
        std::vector<Sphere> spheres;
    };

    static constexpr uint32_t NumBounces = 7;

    struct SceneCastResult
    {
        Material  material;
        QVector3D normal;
        QVector3D point;
        bool      hasHit = false;
    };

    inline static std::mt19937 generator;

    int ImageWidth;
    int ImageHeight;

    int             IntersectRaySphere(QVector3D p, QVector3D d, Sphere s, float& t, QVector3D& q);
    RayCastResult   rayVSSphere(Ray& ray, const Sphere& sphere);
    SceneCastResult RayVsScene(const Scene& scene, Ray ray);
    QVector3D       randomVec();
    QVector3D       diffuse(QVector3D normal);
    QVector3D       Trace(const Scene& scene, const Ray ray);
    QVector3D       reflect(QVector3D dir, QVector3D normal);
    void            render(const Scene& scene, Pixel* image);

  public:
    CPUBasedRayTracingSceneStage();
    ~CPUBasedRayTracingSceneStage() override;
    void        init(std::shared_ptr<ImageProcessorData> given_data) override;
    void        process() override;
    std::string getName() override;
};
