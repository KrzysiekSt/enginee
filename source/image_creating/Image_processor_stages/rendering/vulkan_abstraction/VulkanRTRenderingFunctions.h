#pragma once

#include "Device.h"

#include <QMatrix4x4>
#include <QSize>
#include <QVulkanDeviceFunctions>
#include <QVulkanFunctions>
#include <QVulkanInstance>
#include <algorithm>
#include <fstream>
#include <memory>

// singleton
class VulkanRTRenderingFunctions
{
  private:
  public:
    static VulkanRTRenderingFunctions* getInstance(QVulkanInstance& vulkan);
    ~VulkanRTRenderingFunctions();
    VulkanRTRenderingFunctions(const VulkanRTRenderingFunctions&)            = delete;
    VulkanRTRenderingFunctions& operator=(const VulkanRTRenderingFunctions&) = delete;

    bool                            createWithLayers(const std::vector<std::string>& layers);
    bool                            checkAvailableExtensions(const std::vector<std::string>& looked_extensions);
    std::optional<VkPhysicalDevice> findDevice(std::string name);
    std::optional<unsigned int>     getQueueFamilyIndex(
            const Device& device, const std::vector<VkQueueFlagBits>& queue_properties
        );
    std::optional<VkDevice> createLogicalDevice(
        const Device& device, uint32_t queue_index, const std::vector<std::string>& required_extensions
    );

  private:
    inline static VulkanRTRenderingFunctions* instance = nullptr;
    QVulkanInstance&                          vulkan_context;

    explicit VulkanRTRenderingFunctions(QVulkanInstance& vulkan);

    std::vector<VkExtensionProperties> getVulkanExtensions();
    uint32_t                           getNumberOfExtensions();
    std::vector<VkPhysicalDevice>      getPhysicalDevices();

    uint32_t                              getNumberOfDevices();
    std::vector<VkQueueFamilyProperties2> getAvailableQueueFamilies(const Device& device);
    uint32_t                              getNumberOfQueueFamilies(const Device& device);

    bool checkDeviceExtensions(const Device& device, const std::vector<std::string>& required_extensions);
};
