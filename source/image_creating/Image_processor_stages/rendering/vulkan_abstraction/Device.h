#pragma clang diagnostic push
#pragma ide diagnostic ignored "google-explicit-constructor"
#pragma once

#include <QVulkanDeviceFunctions>
#include <QVulkanFunctions>
#include <QVulkanInstance>

struct Device
{
    VkPhysicalDevice physical_device;
    VkDevice         logical_device;

    operator VkPhysicalDevice() const { return physical_device; }
    operator VkDevice() const { return logical_device; }
};

#pragma clang diagnostic pop