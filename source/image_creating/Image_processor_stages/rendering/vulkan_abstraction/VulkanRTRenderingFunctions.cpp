#include "VulkanRTRenderingFunctions.h"

VulkanRTRenderingFunctions::VulkanRTRenderingFunctions(QVulkanInstance& vulkan) : vulkan_context(vulkan) {}

VulkanRTRenderingFunctions* VulkanRTRenderingFunctions::getInstance(QVulkanInstance& vulkan)
{
    if(instance == nullptr)
    {
        instance = new VulkanRTRenderingFunctions(vulkan);
    }
    return instance;
}

VulkanRTRenderingFunctions::~VulkanRTRenderingFunctions()
{
    delete instance;
    instance = nullptr;
}

bool VulkanRTRenderingFunctions::createWithLayers(const std::vector<std::string>& layers)
{
    vulkan_context.setApiVersion(QVersionNumber {1, 3, 270});
    for(auto& layer: layers)
    {
        if(vulkan_context.supportedLayers().contains(layer.c_str()))
        {
            vulkan_context.setLayers({"VK_LAYER_KHRONOS_validation"});
        }
    }

    bool result = vulkan_context.create();
    if(!result)
    {
        qCritical() << "Failed to create Vulkan instance !";
    }

    return result;
}

bool VulkanRTRenderingFunctions::checkAvailableExtensions(const std::vector<std::string>& looked_extensions)
{
    auto extensions = getVulkanExtensions();
    for(auto& looked_for: looked_extensions)
    {
        bool found = std::find_if(
                         extensions.begin(),
                         extensions.end(),
                         [looked_for](auto& vulkan_extension)
                         { return std::strcmp(vulkan_extension.extensionName, looked_for.c_str()) == 0; }
                     ) != extensions.end();
        if(not found)
        {
            qCritical() << "Vulkan extension not found =" << looked_for;
            for(const auto& extension: extensions)
            {
                qDebug() << "Vulkan extension available =" << extension.extensionName;
            }
            qDebug() << "=============================================";
            return false;
        }
    }

    return true;
}

std::vector<VkExtensionProperties> VulkanRTRenderingFunctions::getVulkanExtensions()
{
    uint32_t number_of_extensions = getNumberOfExtensions();
    qDebug() << "Vulkan available extension number =" << number_of_extensions;
    std::vector<VkExtensionProperties> extensions(number_of_extensions);
    vulkan_context.functions()
        ->vkEnumerateInstanceExtensionProperties(nullptr, &number_of_extensions, extensions.data());

    return extensions;
}

uint32_t VulkanRTRenderingFunctions::getNumberOfExtensions()
{
    uint32_t input_argument_count = 0;
    vulkan_context.functions()->vkEnumerateInstanceExtensionProperties(nullptr, &input_argument_count, nullptr);
    return input_argument_count;
}

std::optional<VkPhysicalDevice> VulkanRTRenderingFunctions::findDevice(std::string name)
{
    std::optional<VkPhysicalDevice> result      = {};
    VkPhysicalDeviceProperties2     device_info = {};
    device_info.sType                           = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;

    auto devices = getPhysicalDevices();
    for(auto& device: devices)
    {
        vulkan_context.functions()->vkGetPhysicalDeviceProperties2(device, &device_info);
        std::string full_name = std::string(device_info.properties.deviceName);
        if(full_name.find(name) != std::string::npos) return device;
    }

    qCritical() << "Device not found =" << name;
    for(auto& device: devices)
    {
        vulkan_context.functions()->vkGetPhysicalDeviceProperties2(device, &device_info);
        qDebug() << "Vulkan device available =" << device_info.properties.deviceName;
    }
    qDebug() << "=============================================";

    return result;
}

std::vector<VkPhysicalDevice> VulkanRTRenderingFunctions::getPhysicalDevices()
{
    uint32_t number_of_devices = getNumberOfDevices();
    qDebug() << "Vulkan available devices number =" << number_of_devices;
    std::vector<VkPhysicalDevice> devices(number_of_devices);
    vulkan_context.functions()
        ->vkEnumeratePhysicalDevices(vulkan_context.vkInstance(), &number_of_devices, devices.data());

    return devices;
}

uint32_t VulkanRTRenderingFunctions::getNumberOfDevices()
{
    uint32_t input_argument_count = 0;
    vulkan_context.functions()->vkEnumeratePhysicalDevices(vulkan_context.vkInstance(), &input_argument_count, nullptr);
    return input_argument_count;
}

std::optional<unsigned int> VulkanRTRenderingFunctions::getQueueFamilyIndex(
    const Device& device, const std::vector<VkQueueFlagBits>& queue_properties
)
{
    std::optional<VkPhysicalDevice> result                    = {};
    auto                            queue_families_properties = getAvailableQueueFamilies(device);
    qDebug() << "Vulkan Queue families properties for device: ";

    bool         gate  = true;
    unsigned int index = 0;
    for(const auto& queue_family_properties: queue_families_properties)
    {
        for(auto& property: queue_properties)
        {
            gate = gate && (queue_family_properties.queueFamilyProperties.queueFlags & property);
        }

        if(gate) return index;
        index++;
    }

    for(const auto& queue_family_properties: queue_families_properties)
    {
        qDebug() << " available = " << queue_family_properties.queueFamilyProperties.queueCount;

        qDebug() << "VK_QUEUE_GRAPHICS_BIT = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) > 0);
        qDebug() << "VK_QUEUE_COMPUTE_BIT = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT) > 0);
        qDebug() << "VK_QUEUE_TRANSFER_BIT = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_TRANSFER_BIT) > 0);
        qDebug() << "VK_QUEUE_SPARSE_BINDING_BIT = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) > 0);
        qDebug() << "VK_QUEUE_PROTECTED_BIT = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_PROTECTED_BIT) > 0);
        qDebug() << "VK_QUEUE_VIDEO_DECODE_BIT_KHR = "
                 << ((queue_family_properties.queueFamilyProperties.queueFlags & VK_QUEUE_VIDEO_DECODE_BIT_KHR) > 0);
        qDebug() << "-----";
    }
    qDebug() << "=============================================";
}

std::vector<VkQueueFamilyProperties2> VulkanRTRenderingFunctions::getAvailableQueueFamilies(const Device& device)
{
    uint32_t number_of_queue_families = getNumberOfQueueFamilies(device);
    qDebug() << "Vulkan available queue families number =" << number_of_queue_families;
    std::vector<VkQueueFamilyProperties2> queue_families_properties(number_of_queue_families);

    for(auto& var: queue_families_properties) var = {VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2};

    vulkan_context.functions()->vkGetPhysicalDeviceQueueFamilyProperties2(
        device,
        &number_of_queue_families,
        queue_families_properties.data()
    );

    return queue_families_properties;
}

uint32_t VulkanRTRenderingFunctions::getNumberOfQueueFamilies(const Device& device)
{
    uint32_t input_argument_count = 0;
    vulkan_context.functions()->vkGetPhysicalDeviceQueueFamilyProperties2(device, &input_argument_count, nullptr);
    return input_argument_count;
}

std::optional<VkDevice> VulkanRTRenderingFunctions::createLogicalDevice(
    const Device& device, uint32_t queue_index, const std::vector<std::string>& required_extensions
)
{
    std::optional<VkDevice> result       = {};
    float                   priority     = 1.f;
    VkDeviceQueueCreateInfo queues_setup = {
        .sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .queueFamilyIndex = queue_index,
        .queueCount       = 1,
        .pQueuePriorities = &priority
    };

    bool passed = checkDeviceExtensions(device, required_extensions);
    if(not passed)
    {
        qCritical() << "device not suitable !";
        return result;
    }

    std::vector<const char*> required_extensions_as_chars;
    for(const auto& ext: required_extensions)
    {
        required_extensions_as_chars.push_back(ext.c_str());
    }

    VkPhysicalDeviceRayTracingPipelinePropertiesKHR ray_tracing_pipeline_properties = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR
    };
    VkPhysicalDeviceProperties2 device_properties = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
        .pNext = &ray_tracing_pipeline_properties
    };
    vulkan_context.functions()->vkGetPhysicalDeviceProperties2(device, &device_properties);

    VkPhysicalDeviceBufferDeviceAddressFeatures buffer_device_address =
        {VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES, nullptr, VK_TRUE};

    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rayTracingPipelineFeatures = {
        .sType              = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR,
        .pNext              = &buffer_device_address,
        .rayTracingPipeline = VK_TRUE
    };

    VkPhysicalDeviceAccelerationStructureFeaturesKHR acceleration_structure_features = {
        .sType                 = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
        .pNext                 = &rayTracingPipelineFeatures,
        .accelerationStructure = VK_TRUE
    };

    VkPhysicalDeviceFeatures2 device_features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = &acceleration_structure_features
    };
    vulkan_context.functions()->vkGetPhysicalDeviceFeatures2(device, &device_features);

    VkDeviceCreateInfo logical_device_setup = {
        .sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext                   = &device_features,
        .queueCreateInfoCount    = 1,
        .pQueueCreateInfos       = &queues_setup,
        .enabledExtensionCount   = static_cast<uint32_t>(required_extensions.size()),
        .ppEnabledExtensionNames = required_extensions_as_chars.data(),
    };

    VkDevice logical_device_input_arg;
    VkResult created =
        vulkan_context.functions()->vkCreateDevice(device, &logical_device_setup, nullptr, &logical_device_input_arg);

    if(created == VK_SUCCESS) result = logical_device_input_arg;
    else
        qCritical() << "Failed to create logical device !";

    return result;
}

bool VulkanRTRenderingFunctions::checkDeviceExtensions(
    const Device& device, const std::vector<std::string>& required_extensions
)
{
    uint32_t device_extension_count;
    vulkan_context.functions()->vkEnumerateDeviceExtensionProperties(device, nullptr, &device_extension_count, nullptr);

    auto device_extensions = std::vector<VkExtensionProperties>(device_extension_count);
    vulkan_context.functions()
        ->vkEnumerateDeviceExtensionProperties(device, nullptr, &device_extension_count, device_extensions.data());

    for(const auto& extension_name: required_extensions)
    {
        bool found = std::find_if(
                         device_extensions.begin(),
                         device_extensions.end(),
                         [extension_name](auto& device_extension)
                         { return std::strcmp(device_extension.extensionName, extension_name.c_str()) == 0; }
                     ) != device_extensions.end();
        if(not found)
        {
            qDebug() << "Extension not found in device : " << extension_name;
            return false;
        }
    }

    return true;
}