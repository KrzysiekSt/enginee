#pragma once

#include "image_creating/essential/Model.h"
#include "image_creating/essential/cameras/PerspectiveCamera.h"

#include <QDebug>
#include <QImage>

class ImageProcessorData
{
  public:
    ImageProcessorData();
    PerspectiveCamera camera = PerspectiveCamera(60, 0.001, 10000000);
    Model*            model;
    QSize             rendering_size = {800, 600};
    QImage            last_render;
};
