#include "ImageProcessor.h"

ImageProcessor::ImageProcessor(std::vector<std::unique_ptr<ImageProcessorStage>> pipeline_order, RenderingApi api) :
    image_processor_iterator(std::move(pipeline_order)), api_used(api)
{}

std::weak_ptr<ImageProcessorData> ImageProcessor::getCurrentRenderingData()
{
    return rendering_data;
}

QImage ImageProcessor::renderFrame()
{
    for(auto& stage: image_processor_iterator)
    {
        stage->process();
    }

    return rendering_data->last_render;
}

void ImageProcessor::assignData(std::shared_ptr<ImageProcessorData> data)
{
    rendering_data = data;
    for(const auto& pipeline_stage: image_processor_iterator)
    {
        pipeline_stage->init(rendering_data);
    }
}
