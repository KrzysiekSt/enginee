#pragma once

#include "ImageProcessorStage.h"

#include <QVector>
#include <QVulkanInstance>
#include <QWeakPointer>

class ImageProcessor
{
  public:
    enum class RenderingApi
    {
        None,
        OpenGL,
        Vulkan
    };

    ImageProcessor(
        std::vector<std::unique_ptr<ImageProcessorStage>> pipeline_order, RenderingApi api = RenderingApi::None
    );
    QImage                            renderFrame();
    void                              assignData(std::shared_ptr<ImageProcessorData> data);
    std::weak_ptr<ImageProcessorData> getCurrentRenderingData();
    RenderingApi                      getApi() { return api_used; }

  private:
    const std::vector<std::unique_ptr<ImageProcessorStage>> image_processor_iterator;
    std::shared_ptr<ImageProcessorData>                     rendering_data;
    RenderingApi                                            api_used;
};
