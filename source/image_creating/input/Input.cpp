#include "Input.h"

std::shared_ptr<ImageProcessorData> Input::getInput()
{
    return data;
}

void Input::addCube()
{
    auto cube_position   = QVector3D {0, 0, -3};
    auto camera_position = QVector3D {0, 0, 0};
    addCubeInternal(cube_position);
    data->camera.setPosition(camera_position);
    QQuaternion look_at = QQuaternion::rotationTo(camera_position, cube_position);
    // QQuaternion::fromDirection((cube_position-camera_position).normalized(), {0,0,1});
    data->camera.setRotation(look_at);
}

void Input::addCubeInternal(
    QVector3D position = {0, 0, 0}, QQuaternion rotation = QQuaternion::fromEulerAngles(0, 0, 0)
)
{
    data->model = &cube;
    data->model->setPosition(position);
    data->model->setRotation(rotation);
}

void Input::addCubeInternal(QVector3D position = {0, 0, 0})
{
    data->model = &cube;
    data->model->setPosition(position);
}

void Input::addCubeInternal(QQuaternion rotation = QQuaternion::fromEulerAngles(0, 0, 0))
{
    data->model = &cube;
    data->model->setRotation(rotation);
}

void Input::addModelFromFile()
{
    // const QString file_path = QFileDialog::getOpenFileName();
    // ObjFileReader::readObjFile(file_path);
}
