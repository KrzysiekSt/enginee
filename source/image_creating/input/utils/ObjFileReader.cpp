#include "ObjFileReader.h"

QVector<Vertex> ObjFileReader::readObjFile(const QString& file_path)
{
    QVector<Vertex> read_vertices;
    //    QFile obj_file(file_path);
    //
    //    tryOpen(obj_file);
    //
    //    QTextStream obj_file_stream(&obj_file);
    //
    //    while (!obj_file_stream.atEnd())
    //    {
    //        QString line = obj_file_stream.readLine().trimmed();
    //        QStringList line_words = line.split(' ', Qt::SkipEmptyParts);
    //
    //        if(isntData(line_words))
    //        {
    //            continue;
    //        }
    //
    //        Vertex vertex;
    //
    //        if (is_position(line_words[0]))
    //        {
    //            if (tokens.size() >= 4)
    //            {
    //                vertex.position = {tokens[1].toFloat(), tokens[2].toFloat(), tokens[3].toFloat()};
    //                vertices.append(vertex);
    //            }
    //        }
    //        else if (tokens[0] == "vn")
    //        {
    //            // Parse vertex normal data
    //            if (tokens.size() >= 4)
    //            {
    //                int lastIndex = vertices.size() - 1;
    //                if (lastIndex >= 0) {
    //                    vertices[lastIndex].nx = tokens[1].toFloat();
    //                    vertices[lastIndex].ny = tokens[2].toFloat();
    //                    vertices[lastIndex].nz = tokens[3].toFloat();
    //                }
    //            }
    //        }
    //        else if (tokens[0] == "vt")
    //        {
    //            // Parse texture coordinate data
    //            if (tokens.size() >= 3) {
    //                int lastIndex = vertices.size() - 1;
    //                if (lastIndex >= 0) {
    //                    vertices[lastIndex].tx = tokens[1].toFloat();
    //                    vertices[lastIndex].ty = tokens[2].toFloat();
    //                }
    //            }
    //        }
    //        else if (tokens[0] == "f")
    //        {
    //            // Parse face data
    //            QVector<int> faceVertexIndices;
    //            for (int i = 1; i < tokens.size(); i++) {
    //                QStringList vertexData = tokens[i].split('/', QString::SkipEmptyParts);
    //                int vertexIndex = vertexData[0].toInt() - 1; // OBJ indices are 1-based
    //                int normalIndex = vertexData[2].toInt() - 1; // OBJ indices are 1-based
    //                faceVertexIndices.append(vertexIndex);
    //
    //                // Store the normal index for this vertex
    //                vertexNormalIndices[vertexIndex].append(normalIndex);
    //            }
    //            // Now, you can access normals for each vertex in faceVertexIndices
    //        }
    //    }
    //
    //    file.close();
    return read_vertices;
}

void ObjFileReader::tryOpen(const QFile& obj_file)
{
    //    if (!obj_file.open(QIODevice::ReadOnly | QIODevice::Text))
    //    {
    //        // error box
    //    }
}

bool ObjFileReader::isntData(const QStringList& line_words)
{
    if(!line_words.isEmpty() and get_data_prefixes().contains(line_words[0])) return false;

    return true;
}