#pragma once

#include "QtCore/QFile"
#include "QtCore/QTextStream"
#include "image_creating/essential/Vertex.h"

class ObjFileReader
{
  public:
    [[nodiscard]] static QVector<Vertex> readObjFile(const QString& file_path);

  private:
    static void                   tryOpen(const QFile& obj_file);
    static bool                   isntData(const QStringList& line_words);
    const static bool             is_position(const QString& word) { return word == "v"; };
    const static bool             is_normal(const QString& word) { return word == "vn"; };
    const static bool             is_coordinate(const QString& word) { return word == "vt"; };
    const static QVector<QString> get_data_prefixes() { return {"v", "vn", "vt"}; };
};
