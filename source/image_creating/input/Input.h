#pragma once

#include "image_creating/ImageProcessorStage.h"
#include "image_creating/essential/Cube.h"
#include "image_creating/essential/Vertex.h"

#include <QOpenGLFunctions_4_5_Core>
#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtGui/QMatrix4x4>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QWidget>

class Input : public QObject
{
    Q_OBJECT
  public:
    std::shared_ptr<ImageProcessorData> getInput();

  public slots:
    void addCube();
    void addModelFromFile();

  private:
    void                                addCubeInternal(QVector3D position, QQuaternion rotation);
    void                                addCubeInternal(QQuaternion rotation);
    void                                addCubeInternal(QVector3D position);
    std::shared_ptr<ImageProcessorData> data = std::make_shared<ImageProcessorData>();
};
