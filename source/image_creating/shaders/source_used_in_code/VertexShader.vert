#version 330 core

layout(location = 0) in vec4 position;
uniform mat4 projection_view;
uniform mat4 model;

void main()
{
    gl_Position = projection_view * model * position;
}