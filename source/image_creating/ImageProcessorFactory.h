#pragma once

#include "ImageProcessor.h"
#include "image_creating/ImageProcessorStage.h"

#include <memory>

class ImageProcessorFactory
{
  public:
    virtual std::shared_ptr<ImageProcessor> createOpenGLImageProcessor() const = 0;
    virtual std::shared_ptr<ImageProcessor> createVulkanImageProcessor() const = 0;
    virtual std::shared_ptr<ImageProcessor> createCPUImageProcessor() const    = 0;
};
