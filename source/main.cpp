#include "WindowManager.h"

#include <QApplication>
#include <QInputDialog>
#include <QtPlugin>

int main(int argc, char* argv[])
{
    QApplication enginee(argc, argv);

    auto rendering_data = WindowManager::createInputWidget();

    QStringList APIs = {"OpenGL", "Vulkan", "CPU"};

    bool    ok;
    QString choosen_api = QInputDialog::getItem(NULL, "Choose Rendering API", "API:", APIs, 0, false, &ok);

    if(ok && !choosen_api.isEmpty())
    {
        if(choosen_api == "OpenGL") WindowManager::createOpenGlRenderingWindow(rendering_data);
        if(choosen_api == "Vulkan") WindowManager::createVulkanRenderingWindow(rendering_data);
        if(choosen_api == "CPU") WindowManager::createCPURenderingWindow(rendering_data);
    }

    WindowManager::startWorkerLoop();

    return enginee.exec();
}
