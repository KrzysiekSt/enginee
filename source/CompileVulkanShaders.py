import os
import subprocess
import sys

if __name__ == "__main__":
    shaders_dir = sys.argv[1]
    print(f"Vulkan shaders compilation started")
    uncompiled_shaders_path = os.path.join(shaders_dir, "uncompiled")
    uncompiled_shader_files = os.listdir(uncompiled_shaders_path)
    shaders_source_path = os.path.join(shaders_dir, "source_used_in_code")

    for uncompiled_shader_file in uncompiled_shader_files:
        input_file_path = os.path.join(uncompiled_shaders_path, uncompiled_shader_file)
        output_file_path = os.path.join(shaders_source_path, os.path.splitext(uncompiled_shader_file)[0] + ".spv")

        result = subprocess.run(["glslangValidator", "--target-env", "vulkan1.3", "-V", "-r", input_file_path, "-o", output_file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if result.returncode != 0:
            print(f"Error compiling {uncompiled_shader_file}:\n{result.stdout.decode('utf-8')}\n{result.stderr.decode('utf-8')}")
        else:
            print(f"Successfully compiled {uncompiled_shader_file} into SPIR-V.")