import sys
import os

def close_file_to_c_string(input_file, output_header):
    try:
        with open(input_file, 'r') as file:
            content = file.read()

        wrapped_content = 'R"(' + content + ')"'

        with open(output_header, 'w') as file:
            file.write(wrapped_content)
        print(f"Content from {input_file} wrapped and saved to {output_header}")

    except Exception as e:
        print(f"Error: {e}")

def close_binary_to_c_pointer(input_file, output_header):
    try:
        with open(input_file, 'rb') as file:
            binary_data = file.read()

        base_name = os.path.splitext(os.path.basename(input_file))[0]
        # Ensure the binary data length is a multiple of 4
        if len(binary_data) % 4 != 0:
            raise ValueError("Binary data length is not a multiple of 4.")

        # Convert the binary data to uint32_t, considering the system's endianness
        words = [binary_data[i:i+4] for i in range(0, len(binary_data), 4)]
        # Reverse the byte order for each 4-byte word in the binary data
        reversed_words = [word[::-1] for word in words]  # Reverse each word
        uint32_array = ", ".join(f"0x{word.hex()}" for word in reversed_words)

        array_content = f"uint32_t {base_name}_shader[] = {{ {uint32_array} }};\n"
        array_length = f"size_t {base_name}_shader_len = sizeof({base_name}_shader);\n"

        with open(output_header, 'w') as file:
            file.write(array_content)
            file.write(array_length)

        print(f"Binary content from {input_file} converted and saved to {output_header}")

    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    shader_dir = sys.argv[1]
    print(f"Shaders wrapping started")
    shaders_source_path = os.path.join(shader_dir, "source_used_in_code")
    shaders_headers_path = os.path.join(shader_dir, "headers")
    shader_filenames = os.listdir(shaders_source_path)


    for shader_file in shader_filenames:
        input_file_path = os.path.join(shaders_source_path, shader_file)
        output_file_path = os.path.join(shaders_headers_path, os.path.splitext(shader_file)[0] + ".h")

        extension = shader_file.split('.')[-1]
        if extension == 'spv':
            close_binary_to_c_pointer(input_file_path, output_file_path)
        else:
            close_file_to_c_string(input_file_path, output_file_path)