#pragma once

#include "gui/InputWidget.h"
#include "gui/TestWindow.h"
#include "image_creating/ImageProcessor.h"
#include "image_creating/image_processor_factories/RasterizationImageProcessorFactory.h"
#include "image_creating/image_processor_factories/RayTracingImageProcessorFactory.h"

#include <QRasterWindow>
#include <QSurface>
#include <QWeakPointer>
#include <QtOpenGL/QOpenGLWindow>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

class WindowManager
{
  public:
    static std::shared_ptr<ImageProcessorData> createInputWidget();
    static void createOpenGlRenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data);
    static void createVulkanRenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data);
    static void createCPURenderingWindow(std::shared_ptr<ImageProcessorData> rendering_data);
    static void clear()
    {
        rendering_window.reset();
        input_widget.reset();
    };
    static void startWorkerLoop();

    inline static std::unique_ptr<TestWindow>  rendering_window = nullptr;
    inline static std::unique_ptr<InputWidget> input_widget     = nullptr;

  private:
    constexpr static RayTracingImageProcessorFactory    rt_factory            = {};
    constexpr static RasterizationImageProcessorFactory rasterization_factory = {};
};
