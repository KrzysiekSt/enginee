# Enginee

### Rendering and image processing framework
### Enginee is free software. You can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the [License](documents/LGPLv3-license.txt), or (at your option) any later version.

![alt text](documentation/control.png)
![alt text](documentation/cube.png)
![alt text](documentation/openGL.png)
![alt text](documentation/cpu.png)
![alt text](documentation/class_diagram.png)

## Used libraries, SDKs and APIs

1. [QT 6.5.3](https://www.qt.io) under license LGPLv3 based on [obligations](https://www.qt.io/licensing/open-source-lgpl-obligations)

    - QT source code: https://github.com/qt/qtbase/tree/v6.5.3
    - License [copy](documents/QT-LGPL-3.0-license.txt) from https://github.com/qt/qtbase/blob/v6.5.3/LICENSES/LGPL-3.0-only.txt

## Requirements
 - python - https://www.python.org/downloads/ - remember to add it to the PATH
 - cmake - https://cmake.org/download/
 - Compiler (msvc / gcc)
 - Vulkan SDK (exe in repo or https://vulkan.lunarg.com/doc/view/1.3.283.0/linux/getting_started.html for linux)

## Get involved
   Any kind of help is appreciated. 
*    If you are a **software developer** look at current [milestone](https://gitlab.com/KrzysiekSt/enginee/-/milestones)
*    If you want to **report a bug** or request a feature, navigate to [issues](https://gitlab.com/KrzysiekSt/enginee/-/issues)
*    If you are a **DevOps guy** take a look at [issues](https://gitlab.com/KrzysiekSt/enginee/-/issues/?label_name%5B%5D=DevOps) labeled with "DevOps"
*    If you specialize in **tests** look for [issues](https://gitlab.com/KrzysiekSt/enginee/-/issues/?label_name%5B%5D=DevOps) labeled with "Tests"
*    If you think that we could use your help, and we are missing something contact me on email krzysztof.stokop@onet.pl or through [discord](https://discord.gg/NpmbeTY2) 

## Build - Windows
1. Clone repository recursively to the shortest path `git clone --recurse-submodules https://gitlab.com/KrzysiekSt/enginee.git D:\Repo`
2. Run `setup.bat` in cmd
3. Run `cmake .`
4. Open project and build the target

## Build - Linux - out of date
1. Clone repository recursively to the shortest path `git clone --recurse-submodules https://gitlab.com/KrzysiekSt/enginee.git D:\Repo`
2. Run `setup.sh`
3. Run `cmake .`
4. Open project and build the target
# ============
1. Build QT (replace your paths) (run with cmd, not in powershell)
 - `cd ./third_party/Qt_source`
 - For some reason I needed some XKB packages to build QT in my case `sudo dnf install libxkbcommon-devel libxkbcommon libxkbcommon-x11-devel libxkbfile-devel xkbcomp xkbcomp-devel xkbset xcb-util-renderutil libxcb xcb-util-xrm-devel xcb-util-wm-devel xcb-util-renderutil-devel xcb-util-keysyms-devel libxcb libxcb-devel xcb-util xcb-util-devel libX11-devel libXrender-devel libxkbcommon-devel libxkbcommon-x11-devel libXi-devel libdrm-devel libXcursor-devel libXcomposite-devel xcb-util-image-devel xcb-util-cursor-devel libxcb-devel libX11-xcb fontconfig-devel fontconfig` solved the problem
 - `./configure -opensource -confirm-license -static -static-runtime -debug -feature-freetype -fontconfig -feature-wayland-server -nomake examples -prefix ../Qt-6.5.3` (we are skipping modules that are not compiling for unknown reason. We probably don't need them anyway)
 - `cmake --build . --parallel`
 - `cmake --install .`
 - `cd ../../`
2. run `cmake .`
3. open project and build the target
